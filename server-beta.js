const https = require('https');
const fs = require('fs');
const Discord = require('discord.js');
const Client = new Discord.Client;
const CredentialsDiscord = require('credentialsDiscord');

const EASY = 1;
const MEDIUM = 2;
const HARD = 3;

const CATEGORY_PAGE = "megaquiz";
const CATEGORY = "MEGA QUIZ";
const CATEGORY_ID = 1;

var lastEmprunteWritingCurrent = "";

var echellePoints = [10,5,2,2];
var niveauDifficulte = MEDIUM;

// Les TIMEOUT
var timeoutDebutPartie = null;
var timeoutFinQuestion = null;
var timeoutQuizFinish = null;
var timeoutQuizContinue = null;
var timeoutAnnonceCandidats = null;
var timeoutAnnonceQuestion = null;
var timeoutNextQuestion = null;
var timeoutRefreshTopMessage = null;

// Last Socket Globale
var lastSocket = null;

// Les personnes en train d'écrire
var currentWritingList = new Array(0);

Client.on("ready", () => {
	
	console.log("bot opérationnel");
	Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).send("Bot Directquiz initialisé sur le salon "+ CATEGORY +" :white_check_mark:");
	
	// Thread Discord
	// Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).threads.cache.get("872963458382053418").send("Bot Directquiz initialisé sur le salon "+ CATEGORY +" :white_check_mark:");	
	
	
	
});

function deleteIfBot(message, channelCurrent)
{
	if (message.author.id == "845315680411582493") channelCurrent.messages.delete(message);				
}

function deleteMessage(message, channelCurrent)
{
	channelCurrent.messages.delete(message);
}

Client.login(CredentialsDiscord.getKey("TOKEN_DISCORD"));

const key = fs.readFileSync('/etc/letsencrypt/live/directquiz.org/privkey.pem');
const cert = fs.readFileSync('/etc/letsencrypt/live/directquiz.org/cert.pem');


var express = require('express');
const axios = require('axios');
var mp3Duration = require('mp3-duration');
const MINUTES_TIMEOUT = 160;
var app = express();
const server1 = https.createServer({ key, cert }, app);
var server = server1.listen(3099);

var socket = require('socket.io');

var io = socket(server);



Client.on("message", messageCommande => {
	
	const channelCurrent = messageCommande.channel; //Client.channels.cache.get(messageCommande.channel.id);
	
	if (messageCommande.content == "!cleardqn")
	{	
		deleteMessage(messageCommande, channelCurrent);
		channelCurrent.messages.fetch({ limit: 100 }).then(messages => {
		messages.forEach(message => deleteIfBot(message, channelCurrent));

		});
	}
	else if(messageCommande.content.indexOf("!send ") == 0)
	{
		var messageIsole = messageCommande.content.replace("!send ","");
		deleteMessage(messageCommande, channelCurrent);
		io.sockets.emit('annonce',{type:111,annonce: "<i><u><strong>[Message envoyé depuis DISCORD] :</strong></u></i> <br/> <img class='microavatar' src='"+messageCommande.author.avatarURL()+"' /> <strong>"+messageCommande.author.username+"</strong> : " + messageIsole.replace(regexHTML,"")  });	
	}

});


const regexHTML = /(<([^>]+)>)/ig;

app.use(express.static('public'));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://www.directquiz.org');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

console.log("Le serveur est effectif !");

var cptQuestion = -1;
var cptReponseDonnee = 0;

var messageTitre = "";

io.sockets.on('connection', newConnection);

io.use(function(socket, next) {
  var handshakeData = socket.request;
  console.log("TOKEN : ", handshakeData._query['token']);
  next();
});

var identifiantPartie = "";

var listeDesMembres = new Array(0);
var listeDesMembresOffline = new Array(0);
var listeDesMembresAvecDetailOffline = new Array(0);
var listeDesMembresAvecDetail = new Array(0);
var listeDesMembresRestant = new Array(0);
var countMember = 0;
var allClients = new Array(0);
var listeRepondantCorrect = new Array(0);
var listeQuestions = new Array(0);
var chrono = null;
var chronoPhHasard = null;
var chronoPhSortieHasard = null;

var chronoDelaiQuestion = null;
var chronoDelairPhraseHasard = null;

var isStart = false;
var isFinish = false;
var listePhrasesInutiles = new Array(0);
var listePhrasesSortie = new Array(0);
var timeout = false;
var globalChronoQuestion = 40;
var timerSecondes = null;

var phrases5secondesRestantes = new Array(0);
var phrasesAttente = new Array(0);

var insultes = new Array(0);

var listePointsWin = new Array(0);

const NOTIFY_CONNECT = 1;
const NOTIFY_DISCONNECT = 2;

 insultes.push("CUL");
 insultes.push("BITE");
 insultes.push("CHIER");
 insultes.push("PUTAIN");
 insultes.push("MERDE"); 
 insultes.push("MERDEUX");
 insultes.push("CONNARD");
 insultes.push("CONNASSE");
 insultes.push("CON");
 insultes.push("CONS");
 insultes.push("FILS DE PUTE");
 insultes.push("SALOPE");
 insultes.push("ENCULES");
 insultes.push("ENCULE");
 insultes.push("ENCULER");
 insultes.push("TA MERE LA PUTE");
 insultes.push("FILS DE CHIEN");
 insultes.push("FOUTRE");
 insultes.push("PUTE");
 insultes.push("PUTTE");
 insultes.push("SALOPE");
 insultes.push("ABRUTI");
 insultes.push("SAPERLIPOPETTE");
 insultes.push("MORVEUX");
 insultes.push("CRETIN");
 insultes.push("IMBECILE");
 insultes.push("FUCK");
 insultes.push("MOTHER FUCKER");
 insultes.push("COUILLES");
 insultes.push("COUILLE");
 insultes.push("ZIZI");
 insultes.push("ZEZETTE");
 insultes.push("KIKINE");

var fileName = "noname.mp3";

init(true);

var aliveIndexList = new Array(0);
var aliveTimeout = null;
var intervalKeepAlive = null;

allClientsDeco = new Array(0);
listeDesMembresDeco = new Array(0);
listeDesMembresAvecDetailDeco = new Array(0);

function keepAlive()
{
	if (allClients.length > 0)
	{	
		// Remise du tableau à 0		
		aliveIndexList = new Array(0);
			
		// Envoyer un Broadcast areAlive()
		io.sockets.emit('are_alive',{value:1});
		
		aliveTimeout = setTimeout(function(){
			
				var totalCli = allClients.length;
			
					for (i = 0; i < totalCli; i++)
					{
						if (aliveIndexList.indexOf(i) == -1)
						{
							allClientsDeco.push(allClients[i]);
							listeDesMembresDeco.push(listeDesMembres[i]);
							listeDesMembresAvecDetailDeco.push(listeDesMembresAvecDetail[i]);
						}
					}
					
				var totalDeco = allClientsDeco.length;			
					
					for (i = 0; i < totalDeco; i++)
					{
						allClientsDeco[i].disconnect();					
						allClients.splice(allClients.indexOf(allClientsDeco[i]), 1);
						listeDesMembres.splice(listeDesMembres.indexOf(listeDesMembresDeco[i]), 1);
						listeDesMembresAvecDetail.splice(listeDesMembresAvecDetail.indexOf(listeDesMembresAvecDetailDeco[i]), 1);
					}
					
				if (listeDesMembres.length > 0)
					refreshListeSlotsInitial();
				else
					init(true);

		},4000);
	}
}

function updateOnlinePlayer(theme, nombre)
{
		const params = new URLSearchParams();
		params.append('theme', theme);
		params.append('nombre', nombre);

		axios({
		  method: 'post',
		  url: 'https://www.directquiz.org/niko.ovh/directquiz89/updateJoueursOnline.php',
		  data: params
		}).then(function (response) {					
			console.log("Nombre de joueurs online mis à jour");
		  })
		  .catch(function (error) {
			console.log(error);
		});
}

///// TEXT-TO-SPEECH /////
async function speech(texteAParler)
{
	const textToSpeech = require('@google-cloud/text-to-speech');
	const util = require('util');
	const ftp = require("basic-ftp");
	const { v4: uuidv4 } = require('uuid');
	
	// Creates a client
	const client = new textToSpeech.TextToSpeechClient();


	  // The text to synthesize
	  var textSpeak = texteAParler.replace("お茶が好き","Oka ga souki").replace(/_/g," ");

	  // Construct the request
	  var request = {
		input: {
		  text: textSpeak
		},
		voice: {
		  languageCode: 'fr-FR',		  
		  ssmlGender: 'MALE',
		  name: "fr-FR-Wavenet-D"
		},
		audioConfig: {
		  audioEncoding: "MP3",
		  speakingRate: 1.20,
		  volumeGainDb: -0.8
		}
	  };


	  // Performs the text-to-speech request
	  const [response] = await client.synthesizeSpeech(request);

	  const writeFile = util.promisify(fs.writeFile);
	   
	   const pathToDelete = '/var/www/html/speech/'+fileName;

		fs.unlink(pathToDelete, (err) => {
		  if (err) {
			console.error(err);
		  }
		});
	   
	   
      fileName = uuidv4()+'.mp3';
	  
      await writeFile('/var/www/html/speech/'+fileName, response.audioContent, 'binary');


mp3Duration('/var/www/html/speech/'+fileName, function (err, duration) {
  if (err) return console.log(err.message);
  io.sockets.emit('annonce_vocale',{type:50, annonce: fileName, secondes: duration});	
});	

}

//////////////////////////






function getUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function timerSec()
{
	globalChronoQuestion--;
	notifyChrono(globalChronoQuestion, 40);
	if (globalChronoQuestion == 0) clearInterval(timerSecondes);
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function phrase5secondesAuHasard()
{
	
	return phrases5secondesRestantes[getRandomInt(0,phrases5secondesRestantes.length)];
	
}

function phraseAttenteAuHasard()
{
	return phrasesAttente[getRandomInt(0,phrasesAttente.length)];
	
}

function lancerUnePhraseAuHasard()
{
	
	var phraseRandom = listePhrasesInutiles[getRandomInt(0,listePhrasesInutiles.length)];
	var membre1 = "une personne";

	var sizeM = listeDesMembres.filter(membre => membre.length > 2).length

	var randomNumber = 0;
	
do { 
randomNumber = getRandomInt(0,listeDesMembres.length);
membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
} while (membre1 === undefined);

	var membre2 = "quelqu'un";
	
	if (isFinish)
	{
		phraseRandom = listePhrasesSortie[getRandomInt(0,listePhrasesSortie.length)];
	}
	else
	{	
		if ((phraseRandom.indexOf("ggg") > -1) && sizeM  > 1)
		{
			

		do { 
					randomNumber = getRandomInt(0,listeDesMembres.length);
					membre1 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
		} while (membre1 === undefined);		
		
				do {
				

					do { 
					randomNumber = getRandomInt(0,listeDesMembres.length);
					membre2 = listeDesMembresAvecDetail[randomNumber].name + (listeDesMembresAvecDetail[randomNumber].version > 1 ?"#"+listeDesMembresAvecDetail[randomNumber].version:"");
					} while (membre2 === undefined);


				}
				while (membre1 == membre2);
			
			
			 phraseRandom = phraseRandom.replace(/mmm/g,membre1).replace(/ggg/g,membre2);
		}
		else
		{
			
			do {
			phraseRandom =  listePhrasesInutiles[getRandomInt(0,listePhrasesInutiles.length)];
			} while (phraseRandom.indexOf("ggg") > -1);

			phraseRandom = phraseRandom.replace(/mmm/g,membre1);
		}
	}

	speech(phraseRandom);	
	io.sockets.emit('annonce',{type:98,annonce:phraseRandom});
		
}

function init(resetMember)
{
	
	try
	{
		clearInterval(intervalKeepAlive);
	}
	catch (error)
	{
		
	}
	
	setTimeout(function(){
		
			intervalKeepAlive = setInterval(
				function(){
		
			keepAlive();
			
		},5000);
		
	},2000);
	

	isStart = false;
	isFinish = false;

	globalChronoQuestion = 40;		
	
	messageTitre = "";

	clearInterval(chronoPhSortieHasard);
	clearInterval(timerSecondes);

	clearTimeout(timeoutDebutPartie);
	clearTimeout(timeoutFinQuestion);
	clearTimeout(timeoutQuizFinish);
	clearTimeout(timeoutQuizContinue);
	clearTimeout(timeoutAnnonceCandidats);
	clearTimeout(timeoutAnnonceQuestion);
	clearTimeout(timeoutNextQuestion);
	clearTimeout(timeoutRefreshTopMessage);
	
	listeRepondantCorrect = new Array(0);
	countMember = 0;
	
	if (resetMember)
	{
		listeDesMembres = new Array(0);
		listeDesMembresAvecDetail = new Array(0);
		listeDesMembresRestant = new Array(0);
		listeDesMembresOffline = new Array(0);
		listeDesMembresAvecDetailOffline = new Array(0);
	}
	else
	{
		isFinish = true;
	}	
	
	cptQuestion = -1;
	cptReponseDonnee = 0;
	chrono = null;
	// var maxQuestionID = 1;
	
	listeQuestions = new Array(0);
	var listeIDQuestions = new Array(0);
	var IDgot = 0;

				const params10 = new URLSearchParams();
				params10.append('niveau', niveauDifficulte);
				params10.append('theme', CATEGORY_ID);

				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/get10QuestionsID.php',
				  data: params10
				}).then(function (response10) {					
					listeIDQuestions = response10.data.split("###");
					
				for (i = 0; i < 10;i++)
				{
					//console.log(">>>"+listeIDQuestions[i]+"<<<");
					
				const params = new URLSearchParams();
				params.append('id', listeIDQuestions[i]);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/getQuestionV2.php',
				  data: params
				}).then(function (response) {
					  //console.log(response.data);
					var resultQuestion = response.data.split("###");
					listeQuestions.push(new question(resultQuestion[0],resultQuestion[1].toUpperCase(), parseInt(resultQuestion[2], 10)));
				  })
				  .catch(function (error) {
					console.log(error);
				  });
					
				}

					
				  })
				  .catch(function (error) {
					console.log(error);
				});

phrasesAttente = new Array(0);

phrasesAttente.push("Bienvenue cher candidat, d'autres joueurs pourraient rejoindre d'un moment à l'autre !");
phrasesAttente.push("Patience pseudo, d'autres candidats devraient arriver...");
phrasesAttente.push("Bonjour pseudo, installe-toi en attendant que les autres candidats rejoignent ce quiz \""+CATEGORY+"\"...");
phrasesAttente.push("Salutation pseudo, certains joueurs ont été notifiés de ta venue, prends patience...");
phrasesAttente.push("Bienvenue pour participer à ce quiz \""+ CATEGORY +"\", pseudo, qui s'annonce excellent et plein de rebondissements !");
phrasesAttente.push("Un tonnerre d'applaudissements pour pseudo qui vient de rejoindre ce quiz \""+ CATEGORY +"\" ! Ah mais je vois qu'il n'y a pas encore d'autres candidats...");


phrases5secondesRestantes = new Array(0);

phrases5secondesRestantes.push("Il ne reste plus que 5 secondes...");
phrases5secondesRestantes.push("Attention, seulement 5 secondes restantes !");
phrases5secondesRestantes.push("Dépéchez-vous, le temps est bientôt écoulé !");
phrases5secondesRestantes.push("5 secondes et le temps sera écoulé...");
phrases5secondesRestantes.push("Encore 5 secondes.");
phrases5secondesRestantes.push("Prudence, plus que 5 secondes");
phrases5secondesRestantes.push("Plus que 5 secondes au chrono.");
phrases5secondesRestantes.push("Le temps est limité, 5 secondes restantes");
phrases5secondesRestantes.push("Faites vite car il ne reste que 5 secondes");
phrases5secondesRestantes.push("Le chrono s'arrète dans 5 secondes");
phrases5secondesRestantes.push("5 secondes");
phrases5secondesRestantes.push("Vite, vite, vite, plus que 5 secondes !");
phrases5secondesRestantes.push("5 secondes, c'est le temps qu'il vous reste !");
phrases5secondesRestantes.push("Ce sont les dernières secondes !");
phrases5secondesRestantes.push("5 secondes restantes !");
phrases5secondesRestantes.push("Il reste peu de temps, faites vite !");
	
listePhrasesInutiles = new Array(0);

listePhrasesInutiles.push("J'ai l'impression que mmm essaie de tricher sur ggg");
listePhrasesInutiles.push("Je me demande ce que nous réserve mmm sur cette partie");
listePhrasesInutiles.push("Apparement, mmm est content d'être là !");
listePhrasesInutiles.push("mmm devrait arrêter de se tourner les pouces et se concentrer sur la partie.");
listePhrasesInutiles.push("mmm est certainement décidé à gagner cette partie !");
listePhrasesInutiles.push("mmm mise tout sur sa victoire...");
listePhrasesInutiles.push("Inutile de préciser que mmm est déterminé à l'emporter sur cette partie...");


listePhrasesInutiles.push("N'oubliez pas de vous laver régulièrement les mains, surtout toi mmm !");
listePhrasesInutiles.push("Prenez exemple sur mmm en toussant dans votre coude...");
listePhrasesInutiles.push("Prière de garder une distance d'un mètre 50 au minimum entre chaque candidats");
listePhrasesInutiles.push("Prenez soin de vos proches et de vous même face à la covid 19...");
listePhrasesInutiles.push("Mes pensées vont aux sinistrés des inondations...");
listePhrasesInutiles.push("Directquiz est un jeu qui demande de l'intuition et de la mémoire");
listePhrasesInutiles.push("mmm, merci de ne pas copier sur votre voisin.");

listePhrasesInutiles.push("mmm et ggg ont l'air déterminés plus que jamais à gagner cette partie !");
listePhrasesInutiles.push("Je pense que mmm versera un petit quelque chose en fin d'émission dans notre tirelire réservée aux victimes des inondations...");

listePhrasesInutiles.push("Je rappelle que des repas seront servis aux candidats après l'émission");
listePhrasesInutiles.push("Des toilettes seront disponibles à la pause, je pense que mmm en a bien besoin");

listePhrasesInutiles.push("Encore merci aux candidats qui se prètent au jeu aujourd'hui dans notre émission");


listePhrasesInutiles.push("Le vaccin est une lueure d'espoir supplémentaire pour mmm et ggg de retrouver une vie normale...");
listePhrasesInutiles.push("mmm s'est surement fait vacciner pour ne pas refiler la covid19 à ggg !");
listePhrasesInutiles.push("Il semblerait que mmm ai bien révisé ses classiques.");
listePhrasesInutiles.push("mmm aimerait être aussi malin que ggg");
listePhrasesInutiles.push("Je pense que mmm est un bon ami de ggg");
listePhrasesInutiles.push("mmm pense surement qu'il va gagner la partie");
listePhrasesInutiles.push("mmm est un redoutable adversaire, il cache bien son jeu...");
listePhrasesInutiles.push("mmm devrait se méfier de ggg...");
listePhrasesInutiles.push("mmm apprécie bien la performance de ggg...");
listePhrasesInutiles.push("mmm veut absolument battre ggg, car il se trouve meilleur que lui...");
listePhrasesInutiles.push("mmm veut faire du mieux qu'il peut");
listePhrasesInutiles.push("mmm a reçu sa deuxième dose du vaccin, il ne peut plus rien lui arriver");
listePhrasesInutiles.push("mmm veuillez penser à remettre correctement votre masque pour ne pas contaminer ggg s'il vous plait");
listePhrasesInutiles.push("mmm est certainement en train de jouer au bureau au lieu de travailler...");
listePhrasesInutiles.push("mmm ne va peut être pas changer le monde, mais brillera certainement lors de cette partie endiablée en compagnie de ggg");
listePhrasesInutiles.push("Serez-vous les coudes, et répondez pour un mieux à la question qui vous est posée.");
listePhrasesInutiles.push("Le savoir forme la sagesse, et la sagesse dessinne un avenir radieux, mmm est bien placé pour le savoir !");
listePhrasesInutiles.push("Je me demande qui de mmm ou ggg va le plus briller lors de cette émission !");
listePhrasesInutiles.push("Il vous reste 20 secondes pour répondre à la question...");
listePhrasesInutiles.push("Les habitants des zones sinistrées, n'ont plus que leurs yeux pour pleurer, aidons-les grâce à la cagnotte mise en place.");
listePhrasesInutiles.push("J'espère que mmm va bien mais en tout cas ggg à l'air de bien veiller sur lui. Remercions ggg de bien veiller sur mmm...");
listePhrasesInutiles.push("Le vaccin commence à couvrir une très bonne quantité de la population de la covid19, bientôt, tout ceci ne sera plus que de l'histoire ancienne.");
listePhrasesInutiles.push("Prière de respecter les gestes barrière entre chaque candidat, je vous ai à l'oeil mmm et ggg !");


listePhrasesSortie = new Array(0);

listePhrasesSortie.push("Veuillez quitter la salle car le quiz est terminé...");
listePhrasesSortie.push("Veuillez sortir du plateau en file indienne...");
listePhrasesSortie.push("Prière de déguerpir avant qu'on ferme !");
listePhrasesSortie.push("Vous prenez vos affaires et vous foutez le camp !");
listePhrasesSortie.push("Si vous ne déguerpissez pas immédiatement, j'appelle les flics !");
listePhrasesSortie.push("Sécurité ! Evacuez-moi ces gens !");
listePhrasesSortie.push("Partez ou relancez une partie mais va falloir prendre une décision.");
listePhrasesSortie.push("L'émission est terminé, déguerpissez.");
listePhrasesSortie.push("du balai !");
listePhrasesSortie.push("De 2 choses l'une, soit vous relancez une partie, soit vous vous en allez...");
listePhrasesSortie.push("Vous pouvez relancer une partie, ou vous en aller, c'est comme vous le voulez.");
listePhrasesSortie.push("Partez ou déguerpissez, le choix vous est donné.");

}

function notify(uuid, displayname, type)
{
	const params = new URLSearchParams();
				params.append('uuid', uuid);
				params.append('displayName', displayname);
				params.append('type', type);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/niko.ovh/directquiz89/notify.php',
				  data: params
				}).then(function (response) {
					 
				  })
				  .catch(function (error) {
					console.log(error);
				  });
}

function question(description, reponse, difficulty)
{
	this.description = description;
	this.reponse = reponse;
	this.difficulty = difficulty;
}

function membre(id, name, tokentime, points, floodCPT, floodTime, floodRespawnTime, floodNbrInfractions, version, lastGame, clientID)
{
	this.id = id;
	this.name = name;
	this.tokentime = tokentime;
	this.points = points;
	this.floodCPT = floodCPT;
	this.floodTime = floodTime;
	this.floodRespawnTime = floodRespawnTime;
	this.floodNbrInfractions = floodNbrInfractions;
	this.version = version;
	this.lastGame = lastGame;
	this.clientID = clientID
}

function itemClassement(uuid, points)
{
	this.uuid = uuid;
	this.points = points;
}

function getIndexOfMemberByPseudo(liste, pseudo)
{
	for (i = 0; i < liste.length ; i++)
	{
		if (liste[i].name == pseudo) return i;
	}
	
	return -1;
}

function nextQuestion()
{
	listeRepondantCorrect = new Array();
	cptReponseDonnee = 0;
	
	
	if (cptQuestion >= (listeQuestions.length-1))
	{
		clearInterval(chrono);
		timeoutFinQuestion = setTimeout(function(){
			isStart = false;
			clearInterval(timerSecondes);
			
			globalChronoQuestion = 40;			
			notifyChrono(0,40);	

			speech("Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !");	

			io.sockets.emit('annonce',{type:4,annonce:"Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !"});
			messageTitre = "Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !";
			
			isFinish = true;
			timeout = true;
				timeoutQuizFinish = setTimeout(function(){
					
					speech("Le quiz est terminé ! Merci à tous pour votre participation !");	
					
					io.sockets.emit('annonce',{type:4,annonce:"Le quiz est terminé ! Merci à tous pour votre participation !"});
					messageTitre = "Le quiz est terminé !";
					updateScoreInDB();
					// (mis au /start) init(false);
					chronoPhSortieHasard = setInterval(lancerUnePhraseAuHasard, 25000);	
					identifiantPartie = "";					
				},6000);

		},250);
	}
	else
	{

		timeoutFinQuestion = setTimeout(function(){
			timeout = true;
			clearInterval(timerSecondes);
			notifyChrono(0,40);

						try{
							
							speech("Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !");	
														
							io.sockets.emit('annonce',{type:4,annonce:"Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !"});			
							messageTitre = "Le temps est écoulé... La bonne réponse était \""+listeQuestions[cptQuestion].reponse.split("|")[0]+"\" !";

						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: "erreur détectée : " + error});
						}

			timeoutQuizContinue = setTimeout(function(){
				cptQuestion++;
				globalChronoQuestion = 40;
				timerSecondes = setInterval(timerSec, 1000);

						try{
							
							resetAffichageClassement();
							
							speech("QUESTION " + (cptQuestion + 1) + " : " + listeQuestions[cptQuestion].description);	
							
							io.sockets.emit('annonce',{type:4,annonce:((niveauDifficulte==HARD)?"🔥 ":"")+"QUESTION " + (cptQuestion + 1) + " : " + listeQuestions[cptQuestion].description});
							messageTitre = "QUESTION " + (cptQuestion + 1) + " : " + listeQuestions[cptQuestion].description;
						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: "erreur détectée : " + error});
						}

				timeout = false;
				
				 chronoDelaiQuestion = setTimeout(nextQuestion,40000);
				 chronoDelairPhraseHasard =  setTimeout(lancerUnePhraseAuHasard,20000);

			},7000);



		},250);		

	}
	
}

function distanceLeven(a, b)
{
    if(!a || !b) return (a || b).length;
    var m = [];
    for(var i = 0; i <= b.length; i++){
        m[i] = [i];
        if(i === 0) continue;
        for(var j = 0; j <= a.length; j++){
            m[0][j] = j;
            if(j === 0) continue;
            m[i][j] = b.charAt(i - 1) == a.charAt(j - 1) ? m[i - 1][j - 1] : Math.min(
                m[i-1][j-1] + 1,
                m[i][j-1] + 1,
                m[i-1][j] + 1
            );
        }
    }

console.log("Distance de Levenshtein entre "+ a +" et "+ b +" : "+ m[b.length][a.length]);

return m[b.length][a.length];
}


function isMatch(msg, reponseAttendue)
{
	var arrayReponse = reponseAttendue.split("|");
	var messageSalted = "";
	var charTemp = "";
	var reponseClean = "";
	var msgClean = "";
	var indiceDeSimilarite = 999;

	// debug ---
	// arrayReponse = new Array(0);
	// arrayReponse.push("l'or");
	// ---

	for (i = 0; i < arrayReponse.length ; i++)
	{

		var reponseDecomposee = arrayReponse[i].split(" ");

		if (reponseDecomposee.length > 1 && (reponseDecomposee[0].toLowerCase().indexOf("d'") == 0 || reponseDecomposee[0].toLowerCase().indexOf("l'") == 0 || reponseDecomposee[0].toLowerCase() == "au" || reponseDecomposee[0].toLowerCase() == "en" || reponseDecomposee[0].toLowerCase() == "le" || reponseDecomposee[0].toLowerCase() == "la" || reponseDecomposee[0].toLowerCase() == "un" || reponseDecomposee[0].toLowerCase() == "une" || reponseDecomposee[0].toLowerCase() == "les" || reponseDecomposee[0].toLowerCase() == "des" || reponseDecomposee[0].toLowerCase() == "du"))
		{
			if (!(reponseDecomposee[0].toLowerCase().indexOf("d'") == 0 || reponseDecomposee[0].toLowerCase().indexOf("l'") == 0))
			{
				reponseDecomposee.splice(0,1);
				reponseClean = reponseDecomposee.join(" ");
			}
			else
			{
				reponseDecomposee[0] = reponseDecomposee[0].substring(2);
				reponseClean = reponseDecomposee.join(" ");
			}
		}
		else
		{	
			if (reponseDecomposee[0].toUpperCase().indexOf("L'") == 0 || reponseDecomposee[0].toUpperCase().indexOf("D'") == 0)
			{
				reponseClean = reponseDecomposee[0].substring(2);
			}
			else
			{
				reponseClean = arrayReponse[i];
			}
		}

		var msgDecompose = msg.split(" ");

		if (msgDecompose.length > 1 && (msgDecompose[0].toLowerCase().indexOf("d'") == 0 || msgDecompose[0].toLowerCase().indexOf("l'") == 0 || msgDecompose[0].toLowerCase() == "au" || msgDecompose[0].toLowerCase() == "en" || msgDecompose[0].toLowerCase() == "le" || msgDecompose[0].toLowerCase() == "la" || msgDecompose[0].toLowerCase() == "un" || msgDecompose[0].toLowerCase() == "une" || msgDecompose[0].toLowerCase() == "les" || msgDecompose[0].toLowerCase() == "des" || msgDecompose[0].toLowerCase() == "du"))
		{
			
			if (!(msgDecompose[0].toLowerCase().indexOf("d'") == 0 || msgDecompose[0].toLowerCase().indexOf("l'") == 0))
			{
				msgDecompose.splice(0,1);
				msgClean = msgDecompose.join(" ");
			}
			else
			{
				msgDecompose[0] = msgDecompose[0].substring(2);
				msgClean = msgDecompose.join(" ");
			}
		}
		else
		{
			if (msgDecompose[0].toUpperCase().indexOf("L'") == 0 || msgDecompose[0].toUpperCase().indexOf("D'") == 0)
			{
				msgClean = msgDecompose[0].substring(2);
			}
			else
			{
				msgClean = msg;
			}
		
		}
		
		msgClean = msgClean.toUpperCase();
		reponseClean = reponseClean.toUpperCase();

		reponseClean = reponseClean.replace(/-/g, " ");
		msgClean = msgClean.replace(/-/g, " ");	

		if (checkIfStrictNumber(reponseClean))
		{
			indiceDeSimilarite = distanceLeven(msgClean.replace(/\D*$/g,''), reponseClean);
			if (indiceDeSimilarite == 0) return true;
		}
		else if (checkIfNumberMesure(reponseClean) && checkIfNumberMesure(msgClean))
		{
			var valeurChiffreeReponseToFind = "";
			var valeurChiffreeReponseSend = "";
			
			var decompositionValeurNommeeReponseToFind = reponseClean.split(" ");
			valeurChiffreeReponseToFind = decompositionValeurNommeeReponseToFind[0];
			decompositionValeurNommeeReponseToFind.splice(0,1);
			var nommageReponseToFind = decompositionValeurNommeeReponseToFind.join(" ");
			
			var decompositionValeurNommeeReponseSend = msgClean.split(" ");
			valeurChiffreeReponseSend = decompositionValeurNommeeReponseSend[0];
			decompositionValeurNommeeReponseSend.splice(0,1);
			var nommageReponseSend = decompositionValeurNommeeReponseSend.join(" ");		
			
			indiceDeSimilariteNommage = distanceLeven(nommageReponseToFind, nommageReponseSend);
			indiceDeSimilariteChiffrage = distanceLeven(valeurChiffreeReponseToFind, valeurChiffreeReponseSend);
	
			if (nommageReponseToFind.length >= 6)
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage <= 3) return true;
			}
			else if (nommageReponseToFind.length < 6 && nommageReponseToFind.length > 2)
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage <= 1) return true;
			}
			else
			{
				if (indiceDeSimilariteChiffrage == 0 && indiceDeSimilariteNommage == 0) return true;
			}
		}
		else
		{
			if (!checkIfNumberMesure(reponseClean))
			{
				indiceDeSimilarite = distanceLeven(msgClean, reponseClean);
				if ((indiceDeSimilarite <= 3 && reponseClean.length >= 8) || (indiceDeSimilarite <= 1 && reponseClean.length < 8 && reponseClean.length > 1) || (indiceDeSimilarite == 0 && reponseClean.length == 1)) return true;
			}
		}	
				
	}
	
	return false;
	
}

function checkIfNumberMesure(str) { 
	var regex = new RegExp(/^\d{1,6} \D+$/); 
	var testResult = regex.test(str.trim()); 
	return testResult;
} 

function checkIfStrictNumber(str) { 
	var regex = new RegExp(/^\d{1,6}$/); 
	var testResult = regex.test(str.trim()); 
	return testResult;
} 

function annonceCandidats()
{
	var arrayPremiersMembresAPresenter = new Array(0);
	var dernierMembreAPresenter = "";
	
	for (i = 0; i < listeDesMembres.length-1; i++)
	{
		arrayPremiersMembresAPresenter.push(listeDesMembres[i]);
	}
	
	dernierMembreAPresenter = listeDesMembres[listeDesMembres.length-1];
	
	speech("Souhaitons la bienvenue aux nouveaux joueurs " + arrayPremiersMembresAPresenter.join(", ") + " et "+ dernierMembreAPresenter +". Dés à présent, que le meilleur gagne !");	
	
	io.sockets.emit('annonce',{type:98,annonce:"Souhaitons la bienvenue aux nouveaux joueurs " + arrayPremiersMembresAPresenter.join(", ") + " et "+ dernierMembreAPresenter +". Dés à présent, que le meilleur gagne !"});
	
}

	function refreshListeSlotsInitial()
	{
		var listeSlots = new Array(0);
		for (i = 0; i < listeDesMembres.length; i++)
		{
			listeSlots.push(listeDesMembresAvecDetail[i]); 
		}
			
		io.sockets.emit('slots', listeSlots);	
	}

function newConnection(socket)
{
	console.log('new connection' + socket.id);
	socket.on('message', messageGet);
	socket.on('currentWriting', currentWriting);
	
	lastSocket = socket;
	socket.on('addMember', addMember);
	socket.on('applaudir', applaudir);
	socket.on('is_alive',isAlive);
	
	notifyChrono(globalChronoQuestion, 40);

		socket.on("disconnect", function(){	

		try
		{
				var i = allClients.indexOf(socket);
				
				console.error("CURRENT ID SOCKET DECO : "+i);
				console.error("TAILLE ARRAY SOCKETS : "+allClients.length);
				
				console.log(listeDesMembresAvecDetail[i].name + " is now offline !");
				listeDesMembresAvecDetailOffline.push(listeDesMembresAvecDetail[i]);
				listeDesMembresOffline.push(listeDesMembres[i]);
				
				var pseudoDeco = listeDesMembres[i];
				var uuidDeco = listeDesMembresAvecDetail[i].id;
				
				socket.broadcast.emit('annonce',{type:2,annonce: listeDesMembres[i] + (listeDesMembresAvecDetail[i].version > 1?"#" + listeDesMembresAvecDetail[i].version:"") +  " s'est déconnecté !"});			
				speech(pseudoDeco + " vient de nous quitter...");
				io.sockets.emit('annonce',{type:98,annonce:pseudoDeco + " vient de nous quitter..."});
			
				allClients.splice(i, 1);
				listeDesMembres.splice(i, 1);
				listeDesMembresAvecDetail.splice(i, 1);
					
				Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).send(":red_circle: `"+pseudoDeco+"` vient de quitter la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length) + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_ETERNALTWIN"));
				
				updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length)

				currentWriting({uuid:uuidDeco, state:0});

				console.log(listeDesMembres.length + " joueur(s) encore connecté...");
				if (listeDesMembres.length > 0)
					refreshListeSlotsInitial();
				else
					init(true);
		}
		catch(error)
		{
			console.error("DISCONNECT ERROR : " + error);	
		}

	});

	function isAlive(data)
	{
		aliveIndexList.push(listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(data.uuid));
	}

	function applaudir(data)
	{
		io.sockets.emit('applaudissement_send',{type:555, user:data.id, annonce: "Vous applaudissez ! 👏"});	
		socket.broadcast.emit('applaudissement_get',{type:555, user:data.id, annonce: data.pseudo + " applaudit de tout son coeur ! 👏"});
	}
		
	function messageGet(data)
	{
		console.log(data.pseudo + " : " + data.message);
		if (data.message.indexOf('giphy.com/media/') > -1)
		{
			var message = data.message;
		}
		else
		{
			var message = data.message.replace(regexHTML,"");
		}
		
		versionPseudo = getMembreDetailByID(data.id).version;
		
		if (versionPseudo > 1)
		{
			data.pseudo = data.pseudo + "#" + versionPseudo;
		}
		
		reponseAttendue = "3598bde5-abe2-4703-a481-384c5276f2b7";
		
		if (cptQuestion >= 0 && cptQuestion < listeQuestions.length) reponseAttendue = listeQuestions[cptQuestion].reponse;
			
		if (message.indexOf("/start") == 0)
		{

			if (listeDesMembres.length >= 2 && !isStart)
			{
					//INIT DE LA DIFFICULTY
					var startingCommande = message.split(" ");
					
					if (startingCommande.length == 1)
					{
						echellePoints = [10,5,2,2];
						niveauDifficulte = MEDIUM;
					}
					else if (startingCommande.length > 1)
					{
						if (startingCommande[1] == "hard")
						{
							echellePoints = [17,10,5,5];
							niveauDifficulte = HARD;
						}
						else if (startingCommande[1] == "easy")
						{
							echellePoints = [6,2,1,1];
							niveauDifficulte = EASY;
						}
						else 
						{
							echellePoints = [10,5,2,2];
							niveauDifficulte = MEDIUM;
						}
					}
				
				// DEBUT
				
				init(false);
				
				isStart = true;
				isFinish = false;
				clearInterval(chronoPhSortieHasard);
				listeRepondantCorrect = new Array();
				timeout = false;				

				timeoutDebutPartie = setTimeout(function(){

					if (cptQuestion == -1)
					{
						cptQuestion++;
						
						for (i = 0; i < listeDesMembresAvecDetail.length; i++)
						{
							listeDesMembresAvecDetail[i].points = 0;
						}
						
						refreshListeSlotsInitial();
						
						speech("Une partie en mode "+ ((niveauDifficulte==MEDIUM)?"normal":((niveauDifficulte==HARD)?"difficile":"facile")) +" va commencer !");	
						
						io.sockets.emit('annonce',{type:4,annonce:"Une partie en mode "+ ((niveauDifficulte==MEDIUM)?"normal":((niveauDifficulte==HARD)?"difficile":"facile")) +" va commencer !"});
						identifiantPartie = getUUID();
						
						
						timeoutAnnonceCandidats = setTimeout(function(){
							
							annonceCandidats();
							

							// Placer les questions par ordre de difficulté
								
								listeQuestions.sort(function (a, b) {
									return a.difficulty - b.difficulty;
								});
								
							var DelaiAvantPremiereQuestion = 3000 + (listeDesMembresAvecDetail.length * 3000); 

							timeoutAnnonceQuestion = setTimeout(function(){
								
								resetAffichageClassement();
								
								speech("QUESTION " + (cptQuestion+1) + " : " + listeQuestions[cptQuestion].description);	
																
								io.sockets.emit('annonce',{type:4,annonce:((niveauDifficulte==HARD)?"🔥 ":"")+"QUESTION " + (cptQuestion+1) + " : " + listeQuestions[cptQuestion].description});
								messageTitre = "QUESTION " + (cptQuestion+1) + " : " + listeQuestions[cptQuestion].description;
								globalChronoQuestion = 40;							
								timerSecondes = setInterval(timerSec, 1000);
									
									
								chronoDelaiQuestion = setTimeout(nextQuestion,40000);
			
													
							},DelaiAvantPremiereQuestion);
						
						},2500);
						

						
						
					}
				},1300);


			}
			else
			{
				if (listeDesMembres.length < 2) 
				{
					io.sockets.emit('annonce',{type:5,annonce:"Il faut plusieurs personnes pour qu'une partie débute..."});
					
				}
				else if (isStart) 
				{
					// io.sockets.emit('annonce',{type:98,annonce:"La partie est déjà lancée !"});
				}
			}
		}
		else if (!timeout && (!isFinish && isStart && (isMatch(message.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), reponseAttendue.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) && cptQuestion >= 0)))
		{
			
				var pts = 0;

				if (listeRepondantCorrect.lastIndexOf(data.pseudo) == -1)
				{
					cptReponseDonnee++;
					
					switch (cptReponseDonnee)
					{
						case 1 : pts = echellePoints[0]; break;
						case 2 : pts = echellePoints[1]; break;
						case 3 : pts = echellePoints[2]; break;
						case 4 : pts = echellePoints[3]; break;
						default : pts = 0;					
					}

					if (pts > 0)
					{

						try
						{
							var indexToAddPoints = listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(data.id); //listeDesMembres.indexOf(data.pseudo);
							listeDesMembresAvecDetail[indexToAddPoints].points += pts;
							listeDesMembresAvecDetail[indexToAddPoints].lastGame = identifiantPartie;
							
							notifyPointsWin(new itemClassement(data.id, pts));
							notifyOrdreBonneReponse({uuid:data.id, ordre:cptReponseDonnee});
							
							io.sockets.emit('annonce_user',{type:3, user:data.id, annonce: "Bonne réponse ! "+pts+" points gagnés !"});	
							socket.broadcast.emit('annonce',{type:3, annonce: data.pseudo + " a trouvé la bonne réponse ! "+pts+" points gagnés !"});
							listeRepondantCorrect.push(data.pseudo);
							refreshListeSlots();
							
							// Stoppage si tout le monde ou 4 personnes ont trouvé
								
								if (listeRepondantCorrect.length == 4 || listeRepondantCorrect.length == listeDesMembres.length)
								{
									clearTimeout(chronoDelaiQuestion);
									clearTimeout(chronoDelairPhraseHasard);

									timeout = true;
									clearInterval(timerSecondes);
									notifyChrono(0,40);
								
									speech("Toutes les réponses ont été trouvées...");
									io.sockets.emit('annonce',{type:4,annonce:"Toutes les réponses ont été trouvées..."});
	
									timeoutNextQuestion = setTimeout(nextQuestion, 6500);
								}
							
							
							
						}
						catch(error)
						{
							console.error(error);
							socket.broadcast.emit('annonce',{type:3,annonce: "erreur détectée : " + error});
						}

					}
				}
				else // Cas réponse déjà trouvée par l'utilisateur qui écris un message
				{
					io.sockets.emit('annonce_user',{type:3, user:data.id, annonce: "Vous avez déjà trouvé la bonne réponse !"});
				}
		}
		else
		{

				if (message.indexOf("/speak") == 0)
				{
					speech(message.replace("/speak",""));			
				}				
				else if (message.indexOf("/kick ") == 0)
				{
					var commande = message.split(" ");
					kick(commande[1]);
				}
				else
				{
					
					var tempMessage = message;				
					var motsMessages = tempMessage.split(" ");
					var indexInsulte = -1;
					
					for (i = 0; i < motsMessages.length; i++)
					{
						indexInsulte = insultes.indexOf(motsMessages[i].normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase());
						
						if (indexInsulte > -1)
						{		
							var censure = "";
							for (j = 0; j < insultes[indexInsulte].length; j++)
							{
								censure += "*";
							}
							motsMessages[i] = censure;	
						}
					}
					
					data.message = motsMessages.join(" ");									
					
					socket.broadcast.emit('message', data);
				}				
				
			
			
		}
		
		
	}
	
	function addMember(data)
	{

		var clientID = data.client_id; 
		var id = data.id;
		var pseudo = data.pseudo;
		var socket_ = lastSocket;
	
	
		var pseudo = pseudo.replace(/ /g, "");
	
		console.log("Adding member : " + pseudo);
		var re = "";
		var dataAckMaxUser = {msg:"ERROR_TOOMANYUSER"};
		var dataAckExist = {msg:"ERROR_ALREADYEXIST"};
		var dataAckOK = {msg:"OK"};
		
		var indexMemberFound = listeDesMembresAvecDetail.map(function(e) { return e.id; }).indexOf(id);
		
		if (listeDesMembres.length == 8)
		{
			console.log("REFUS 8 MAX");
			socket.emit('ack', dataAckMaxUser);
			socket.disconnect(true);
		}
		else if (indexMemberFound > -1)
		{
			
			console.log("EJECTION MEMBRE EXISTE - CLIENTID="+listeDesMembresAvecDetail[indexMemberFound].clientID);
			io.sockets.emit('forcer_ejection', {client_id:listeDesMembresAvecDetail[indexMemberFound].clientID});
			
			setTimeout(function(){
				
							console.log("ACCES AUTORISE");
							socket.emit('ack', dataAckOK);
							
							try
							{
								var idOfficielOffline = listeDesMembresAvecDetailOffline.map(function(e) { return e.id; }).indexOf(id);
							}
							catch (ex)
							{
								var idOfficielOffline = -1;
							}
							
							if (idOfficielOffline > -1)
							{
								re = "re";
								var returnMembre = listeDesMembresAvecDetailOffline[idOfficielOffline];
								returnMembre.clientID = clientID;
								returnMembre.tokentime = Math.floor(Date.now() / 1000);
								listeDesMembresOffline.splice(i, 1);
								listeDesMembresAvecDetailOffline.splice(i, 1);
								
								if (returnMembre.lastGame != identifiantPartie)
								{
									returnMembre.points = 0;
								}
								
								listeDesMembresAvecDetail.push(returnMembre);
								allClients.push(socket_);										
							}
							else
							{	

								var identicalNickname = 1;
								for (i = 0; i < listeDesMembres.length; i++)
								{
									if (listeDesMembres[i] == pseudo)
									{
										identicalNickname++;
									}
								}
						
								listeDesMembresAvecDetail.push(new membre(id, pseudo,Math.floor(Date.now() / 1000),0,0,Math.floor(Date.now()), 0, 0, identicalNickname, identifiantPartie, clientID));
								allClients.push(socket_);
							}
							
							listeDesMembres.push(pseudo);
							notifyChrono(globalChronoQuestion,40);
							
							getMembreDetailByID();
							
							countMember = listeDesMembres.length;
							socket.broadcast.emit('annonce',{type:1,annonce:pseudo + (getMembreDetailByID(id).version > 1?"#" + getMembreDetailByID(id).version:"") + " s'est " + re + "connecté !"});
							
							if (countMember > 1)
							{
								speech( (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo ));
								io.sockets.emit('annonce',{type:98,annonce: (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo )  });
							}
							else
							{
								var phraseAccueil = phraseAttenteAuHasard().replace("pseudo",pseudo);
								speech( phraseAccueil);
								io.sockets.emit('annonce',{type:98,annonce: phraseAccueil});
							}							
														
							Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).send(":green_circle: `"+pseudo+"` vient de rejoindre une partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_ETERNALTWIN"));
							
							updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length);
							refreshListeSlotsInitial();	

							setTimeout(function(){
				
									io.sockets.emit('refresh_status_writing',{id:currentWritingList});
								
							},800);
							
							timeoutRefreshTopMessage = setTimeout(function(){io.sockets.emit('refreshTopMessage',{nickname:pseudo, msg:messageTitre});},2000);
						
				
			},1500);

		}
		else
		{	
			// [CODE DUPLIQUE __^]
			console.log("ACCES AUTORISE");
			socket.emit('ack', dataAckOK);
			
			try
			{
				var idOfficielOffline = listeDesMembresAvecDetailOffline.map(function(e) { return e.id; }).indexOf(id);
			}
			catch (ex)
			{
				var idOfficielOffline = -1;
			}
			
			if (idOfficielOffline > -1)
			{
				re = "re";
				var returnMembre = listeDesMembresAvecDetailOffline[idOfficielOffline];
				returnMembre.clientID = clientID;
				returnMembre.tokentime = Math.floor(Date.now() / 1000)
				listeDesMembresOffline.splice(i, 1);
				listeDesMembresAvecDetailOffline.splice(i, 1);
				
				if (returnMembre.lastGame != identifiantPartie)
				{
					returnMembre.points = 0;
				}
				
				listeDesMembresAvecDetail.push(returnMembre);
				allClients.push(socket_);
			}
			else
			{	

				var identicalNickname = 1;
				for (i = 0; i < listeDesMembres.length; i++)
				{
					if (listeDesMembres[i] == pseudo)
					{
						identicalNickname++;
					}
				}
		
				listeDesMembresAvecDetail.push(new membre(id, pseudo,Math.floor(Date.now() / 1000),0,0,Math.floor(Date.now()), 0, 0, identicalNickname, identifiantPartie, clientID));
				allClients.push(socket_);
			}
			
			listeDesMembres.push(pseudo);
			notifyChrono(globalChronoQuestion,40);
			
			getMembreDetailByID();
			
			countMember = listeDesMembres.length;
			socket.broadcast.emit('annonce',{type:1,annonce:pseudo + (getMembreDetailByID(id).version > 1?"#" + getMembreDetailByID(id).version:"") + " s'est " + re + "connecté !"});
			
			if (countMember > 1)
			{
				speech( (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo ));
				io.sockets.emit('annonce',{type:98,annonce: (re == "re"? pseudo + " est de retour parmi nous":"Souhaitons la bienvenue à " + pseudo )  });
			}
			else
			{
				var phraseAccueil = phraseAttenteAuHasard().replace("pseudo",pseudo);
				speech( phraseAccueil);
				io.sockets.emit('annonce',{type:98,annonce: phraseAccueil});
			}
			
			
						
			Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).send(":green_circle: `"+pseudo+"` vient de rejoindre une partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + listeDesMembres.length + "** joueur"+((listeDesMembres.length>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_ETERNALTWIN"));
			
			updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length)
			refreshListeSlotsInitial();	
			
			setTimeout(function(){
				
				io.sockets.emit('refresh_status_writing',{id:currentWritingList});
				
			},800);
			
			
			timeoutRefreshTopMessage = setTimeout(function(){io.sockets.emit('refreshTopMessage',{nickname:pseudo, msg:messageTitre});},2000);
		
		}
		
	}
	
	function getMembreDetailByID(uuid)
	{
		for (i = 0; i < listeDesMembresAvecDetail.length; i++)
		{
			if (listeDesMembresAvecDetail[i].id == uuid)
			{
				return listeDesMembresAvecDetail[i];
			}
		}
		
		return null;
	}
	
	function refreshListeSlots()
	{
		var listePoints = new Array(0);
		for (i = 0; i < listeDesMembres.length; i++)
		{
			listePoints.push(new itemClassement(listeDesMembresAvecDetail[i].id, listeDesMembresAvecDetail[i].points)); 
		}
		
		io.sockets.emit('slotsClassement', listePoints);
	}
	

	

}

	function purgeMember(i)
	{
		// Ne fonctionne pas bien (kick)
		/*
		if (listeDesMembres[i] !== undefined)
		{
			
			if (listeDesMembresAvecDetailOffline.indexOf(listeDesMembresAvecDetail[i]) == -1)
			{
				listeDesMembresAvecDetailOffline.push(listeDesMembresAvecDetail[i]);
				listeDesMembresOffline.push(listeDesMembres[i]);
				
				var pseudoDeco = listeDesMembres[i];
			
			// // Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_PATBOT")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_PATBOT")).send(":red_circle: `"+pseudoDeco+"` vient d'être kické de la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length - 1) + "** joueur"+(((listeDesMembres.length - 1)>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_PATBOT"));
			// Client.guilds.cache.get(CredentialsDiscord.getKey("GUILD_ID_ETERNALTWIN")).channels.cache.get(CredentialsDiscord.getKey("CHANNEL_ID_ETERNALTWIN")).send(":red_circle: `"+pseudoDeco+"` vient d'être kické de la partie "+ CATEGORY +" sur https://www.directquiz.org/"+ CATEGORY_PAGE +".php 🎮 | **" + (listeDesMembres.length - 1) + "** joueur"+(((listeDesMembres.length - 1)>1)?"s":"")+" en ligne | "+CredentialsDiscord.getKey("ROLE_DIRECTQUIZ_ETERNALTWIN"));
			updateOnlinePlayer(CATEGORY_ID, listeDesMembres.length-1)			
		
			allClients.splice(i, 1);
			listeDesMembres.splice(i, 1);
			listeDesMembresAvecDetail.splice(i, 1);
					
			io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});		
					
			console.log(allClients.length + " joueur(s) encore connecté...");
				
			if (allClients.length > 0)
				refreshListeSlots();
			else
				init(true);	

			}		
			
		}
		*/
	}

	function kick(pseudo)
	{
		var arrayPseudo = pseudo.split("#");

		if (arrayPseudo.length > 1)
		{	
			for (i = 0; i < listeDesMembres.length; i++)
			{
				
					if (listeDesMembresAvecDetail[i].name.toUpperCase() == arrayPseudo[0].toUpperCase() && listeDesMembresAvecDetail[i].version+"" == arrayPseudo[1])
					{
						io.sockets.emit('annonce',{type:5,annonce:listeDesMembres[i] + (listeDesMembresAvecDetail[i].version > 1?"#"+listeDesMembresAvecDetail[i].version:"") + " a été expulsé de la partie..."});
						//listeDesMembresAvecDetail[i].points = 0;
						io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});
						
						purgeMember(i);
					}

			}
		}
		else
		{
			for (i = 0; i < listeDesMembres.length; i++)
			{
				
					if (listeDesMembresAvecDetail[i].name.toUpperCase() == arrayPseudo[0].toUpperCase())
					{
						io.sockets.emit('annonce',{type:5,annonce:listeDesMembres[i] + " a été expulsé de la partie..."});
						//listeDesMembresAvecDetail[i].points = 0;
						io.sockets.emit('kick',{id:listeDesMembresAvecDetail[i].id});

						purgeMember(i);
					}

			}
		}
	}

	function notifyChrono(restant_, total_)
	{
	
		if (restant_ == 6)
		{			
				var phrase5Secondes = phrase5secondesAuHasard();
				speech(phrase5Secondes);	
				io.sockets.emit('annonce',{type:98,annonce:phrase5Secondes});
		}
		
		io.sockets.emit('chrono',{total:total_,restant:restant_});
	}
	
	function resetAffichageClassement()
	{
		var data = "reset";
		io.sockets.emit('reset_affichage_classement',data);
	}
	
	function notifyPointsWin(data)
	{
		io.sockets.emit('notify_points',data);
	}
	
	function notifyOrdreBonneReponse(data)
	{
		io.sockets.emit('notify_ordre',data);
	}
	
	function currentWriting(data)
	{
		var i = currentWritingList.indexOf(data.uuid)
		
		if (data.state == 1)
		{
			if (i == -1) currentWritingList.push(data.uuid)
		}
		else
		{
			if (i != -1) currentWritingList.splice(i, 1);	
		}

		if (lastEmprunteWritingCurrent != JSON.stringify(currentWritingList))
		{
			lastEmprunteWritingCurrent = JSON.stringify(currentWritingList);
			io.sockets.emit('refresh_status_writing',{id:currentWritingList});
		}

	}
	
	function updateScoreInDB()
	{
		for (i = 0; i < listeDesMembresAvecDetail.length; i++)
		{
			
			try 
			{
				const params = new URLSearchParams();
				params.append('uuid', listeDesMembresAvecDetail[i].id);
				params.append('points', listeDesMembresAvecDetail[i].points);
				axios({
				  method: 'post',
				  url: 'https://www.directquiz.org/directquiz.niko.ovh/dev/ajax/score101.php',
				  data: params
				}).then(function (response) {
						console.log("Membre \""+ listeDesMembresAvecDetail[i].name + "\" --> Score enregistré avec succès dans la DB !");
				  })
				  .catch(function (error) {
					console.log(error);
				  });
			}
			catch (error)
			{
				console.error(error);
			}
		}
	}
