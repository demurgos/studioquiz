var socket;
var pseudoClient = "UNKNOWN";
var currentUUID = "";

var audioIntroGame = new Audio('https://directquiz.niko.ovh/dev/intro-game.mp3');

var audioBonneReponse = new Audio('https://directquiz.niko.ovh/dev/bip_bonne_reponse.mp3');
var audioLettre = new Audio('https://directquiz.niko.ovh/dev/bip_lettre.mp3');
var audioTic = new Audio('https://directquiz.niko.ovh/dev/tic.mp3');
var audioTac = new Audio('https://directquiz.niko.ovh/dev/tac.mp3');
var audioFin  = new Audio('https://directquiz.niko.ovh/dev/elapsed-modern.mp3');
var audioFlood  = new Audio('https://directquiz.niko.ovh/dev/bip_flood.mp3');
var audioQuestion  = new Audio('https://directquiz.niko.ovh/dev/question.mp3');

var audioMsg  = new Audio('https://directquiz.niko.ovh/dev/bip_msg.mp3');

var audioApplause  = new Array(0);

var typingTimer;
var isTyping = false;

// FOND PLEIN : #f3feee
// FOND NORMAL : #d1f9fc
// FOND 10 Secondes restantes : #ffece4
var PROGRESS_BAR_BACK_COLOR_HIGH = "#f3feee";
var PROGRESS_BAR_BACK_COLOR_MEDIUM = "#d1f9fc";
var PROGRESS_BAR_BACK_COLOR_LOW = "#ffece4";

var PROGRESS_BAR_BACK_COLOR_ULTRALOW = "#ffabab";

// BARRE PLEIN : #8dd280
// BARRE NORMAL : #3d98d2
// BARRE 10 Secondes restantes : #eb7c70
var PROGRESS_BAR_COLOR_HIGH = "#8dd280";
var PROGRESS_BAR_COLOR_MEDIUM = "#3d98d2";
var PROGRESS_BAR_COLOR_LOW = "#eb7c70";

var PROGRESS_BAR_COLOR_ULTRALOW = "#f51313";

var currentApplause = -1;

var audioConnected  = new Audio('https://directquiz.niko.ovh/dev/connected.mp3');
var audioDisconnected  = new Audio('https://directquiz.niko.ovh/dev/disconnected.mp3');

var audioBlabla  = new Audio('https://directquiz.niko.ovh/dev/blabla.mp3');
const regexHTML = /(<([^>]+)>)/ig;

var actualChar = 0;
var currentText = "";

var currentQuestionOrTimeOut = "";
var intervaleChangephrase = null;
var annonceSpeaked = "";
var socket = null;

var newClassement = null;

var ranksTop = null;
var ranksLeft = null;

var points = null;

var rankingRestant = 8;

var dataMember = null;

var currentSalon = 0;

var isOnline = true;

var currentpseudonyme = null;
var currentUUID = null;
var currentclientID = null;
var currentSalon = null;

/*
phrases5secondesRestantes = new Array(0);

phrases5secondesRestantes.push("Il ne reste plus que 5 secondes...");
phrases5secondesRestantes.push("Attention, seulement 5 secondes restantes !");
phrases5secondesRestantes.push("Depechez-vous, le temps est bientôt écoulé !");
phrases5secondesRestantes.push("5 secondes et le temps sera écoulé...");
phrases5secondesRestantes.push("Encore 5 secondes.");
phrases5secondesRestantes.push("Prudence, plus que 5 secondes !");
phrases5secondesRestantes.push("Plus que 5 secondes au chrono.");
phrases5secondesRestantes.push("Le temps est limité, 5 secondes restantes !");
phrases5secondesRestantes.push("Prudence, le temps restant n'est plus que de 5 secondes !");
phrases5secondesRestantes.push("Faites vite car il ne reste que 5 secondes !");
phrases5secondesRestantes.push("Le chrono s'arrète dans 5 secondes !");
phrases5secondesRestantes.push("5 secondes !");

function phrase5secondesAuHasard()
{
	
	return phrases5secondesRestantes[getRandomInt(0,phrases5secondesRestantes.length)];
	
}
*/

function getTypeMessage(IDAnnonce)
{
	switch (IDAnnonce)
	{
		case 1 : return '-connect'; 
		case 2 : return '-disconnect'; 
		case 3 : return '-good-answer'; 
		case 4 : return '-presenter'; 
		case 5 : return '-info'; 
		case 6 : return '-info'; 
		case 98 : return '-presenter'; 
		case 111 : return '-info';// DISCORD
		case 555 : return '-clap'; 
	}

}

for (i=0;i<24;i++)
{
	audioApplause.push(new Audio('https://directquiz.niko.ovh/dev/applause'+(i+1)+'.mp3'))
}

function nextApplause()
{	
	if (currentApplause == 23) currentApplause = 0;
	else currentApplause++;
	
	return currentApplause;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

String.prototype.normalizeAcc = function(){
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
     
    return str;
}

function connectToServer(pseudonyme, iduser, salon, clientID)
{

	currentpseudonyme = pseudonyme;
	currentUUID = iduser;
	currentclientID = clientID;
	
	pseudonyme = pseudonyme.replace(/ /g,"");
	
	if (salon == 1)
		socket = io.connect('www.directquiz.org:3000', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b7", transports: ["websocket"]});
	else if (salon == 2)
		socket = io.connect('www.directquiz.org:3001', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b8", transports: ["websocket"]});
	else if (salon == 3)
		socket = io.connect('www.directquiz.org:3002', {query: "token=3598bde5-abe2-4703-a481-384c5276f2b9", transports: ["websocket"]});
	else if (salon == 4)
		socket = io.connect('www.directquiz.org:3003', {query: "token=3598bde5-abe2-4703-a481-384c5276f2ba", transports: ["websocket"]});
	else if (salon == 5)
		socket = io.connect('www.directquiz.org:3004', {query: "token=3598bde5-abe2-4703-a481-384c5276f2bb", transports: ["websocket"]});
	else if (salon == 6)
		socket = io.connect('www.directquiz.org:3005', {query: "token=3598bde5-abe2-4703-a481-384c5276f2bd", transports: ["websocket"]});
	else if (salon == 99)	
		socket = io.connect('www.directquiz.org:3050', {query: "token=3598bde5-abe2-4703-a481-384c5276f2bc", transports: ["websocket"]});
		
	currentSalon = salon;
		
	socket.on('message', newMessage);
	socket.on('annonce_vocale', newAnnonceVocale);
	socket.on('annonce', newAnnonce);
	socket.on('annonce_user', newAnnonceUser);
	socket.on('slots', newListeSlots);
	socket.on('slotsClassement', newListeSlotsClassement);
	// socket.on('checkDisconnect', checkDisconnect);		
	socket.on('ack', acknowledge);
	socket.on('refreshTopMessage', refreshTopMessage);
	socket.on('chrono', updateChrono);
	socket.on('annonce_flood_user', floodGet);
	
	socket.on('applaudissement_send', applaudissement_send);
	socket.on('applaudissement_get', applaudissement_get);

	socket.on('notify_points',notifyPoints);
	socket.on('notify_ordre',notifyOrdre);
	
	socket.on('reset_affichage_classement', resetAffichageClassement);

	socket.on('kick', kick);		
	socket.on('forcer_ejection', forcerEjection);
	
	socket.on('refresh_status_writing', refreshStatusWriting);
	
	// socket.on('are_alive', areAlive);
	
	dataMember = {
		    id: iduser,
			pseudo: pseudonyme,
			client_id : clientID
				};			
	
	socket.emit('addMember',dataMember);
	
	pseudoClient = pseudonyme;
	
}

/*
function areAlive(data)
{
	if (data.value == 1)
	{
		socket.emit('is_alive',{uuid:currentUUID});
	}
}
*/

function kick(data)
{
	if (IDConnected == data.id)
	{
		window.location = "https://www.directquiz.org/index.php?type=kick";
	}
}

function displayTexteProgressif() {
		actualChar = 0;
	    intervaleChangephrase = setInterval('changeMessage()', 90);
}

function changeMessage() {
	     if (actualChar < currentQuestionOrTimeOut.length) {
	         currentText += currentQuestionOrTimeOut.charAt(actualChar);	      
	         actualChar++;
			 if (!isMuted) audioLettre.play();
	     }
	     $('#questionbox-content').text(currentText);
		 
		 if (actualChar == currentQuestionOrTimeOut.length)
		 {
			 clearInterval(intervaleChangephrase);			 
		 }
}
	 
	 
function write(data)
{
	
	newMessageClient(data);
	
	if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") == -1)
	{	
		if (data.message.indexOf("/gif") == -1 && data.message != "/mute"  && data.message != "/unmute" && data.message != "/cls" && data.message.indexOf("/setvoice") == -1) socket.emit('message',data);	
	}	
	
	$("#message-send").val("");
	
}

function acknowledge(data)
{

	if (data.msg == "ERROR_TOOMANYUSER")
	{
		window.location = '8max.php';
	}
	else if (data.msg == "OK")
	{
		$("#currentConnected").text(pseudoClient);
		$("#connectFrame").hide();
		$("#sender").show();
	}
	
}

function refreshTopMessage(data)
{
	if (data.nickname == pseudoClient) $("#questionbox-content").html(data.msg);
}

function floodGet(data)
{
	if (data.user == pseudoClient)
	{
		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.message + "</div>");
		$("#chatbox").scrollTop(99999);
		audioFlood.play();
	}	
}

function updateChrono(data)
{
	// data.restant
	// data.total
				if (parseInt(data.restant,10) < 40)
				{
					if (parseInt(data.restant,10) == 0)
					{
						if (!isMuted) audioFin.play();
					}
					else
					{
						if (!isMuted)
						{
							if (parseInt(data.restant,10)%2 == 0) audioTic.play();
							else audioTac.play();
							
							// if (parseInt(data.restant,10) == 5) presentateurParle(phrase5secondesAuHasard());
						}
						
					}
				}
			
			var color_bar_progress = "#8dd280";
			var color_back_progress = "#f3feee";
			
			if (data.restant <= 40 && data.restant >= 30)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_HIGH;
				color_bar_progress = PROGRESS_BAR_COLOR_HIGH;
			}
			else if (data.restant <= 29 && data.restant >= 11)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_MEDIUM;
				color_bar_progress = PROGRESS_BAR_COLOR_MEDIUM;
			}
			else if (data.restant >= 6 && data.restant <= 10)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_LOW;
				color_bar_progress = PROGRESS_BAR_COLOR_LOW;
			}
			else if (data.restant >= 0 && data.restant <= 5)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_ULTRALOW;
				color_bar_progress = PROGRESS_BAR_COLOR_ULTRALOW;
			}			
				$(".progress-circle").css("border-color",color_back_progress) // Couleur du fond de l'anneau
				$(".progress-barre").css("border-color",color_bar_progress) // Couleur de la progress bar
				$(".progress-sup50").css("border-color",color_bar_progress) // Couleur de la progress bar

			
		$(".progress-circle").each(function(){			
			$(this).attr("data-value",(data.restant < 10?"0":"")+data.restant);	

			if (data.restant == 0)
			{
				setTimeout(function(){
					
					$(this).attr("data-value","40");
					
				},1000);
			}			
		}); 
		

	

}

function nettoyerMessageDeToutesSesInsultes(message)
{
	insultes = new Array(0);
	
 insultes.push("CUL");
 insultes.push("BITE");
 insultes.push("CHIER");
 insultes.push("PUTAIN");
 insultes.push("MERDE"); 
 insultes.push("MERDEUX");
 insultes.push("CONNARD");
 insultes.push("CONNASSE");
 insultes.push("CON");
 insultes.push("CONS");
 insultes.push("FILS DE PUTE");
 insultes.push("SALOPE");
 insultes.push("ENCULES");
 insultes.push("ENCULE");
 insultes.push("ENCULER");
 insultes.push("TA MERE LA PUTE");
 insultes.push("FILS DE CHIEN");
 insultes.push("FOUTRE");
 insultes.push("PUTE");
 insultes.push("PUTTE");
 insultes.push("SALOPE");
 insultes.push("ABRUTI");
 insultes.push("SAPERLIPOPETTE");
 insultes.push("MORVEUX");
 insultes.push("CRETIN");
 insultes.push("IMBECILE");
 insultes.push("FUCK");
 insultes.push("MOTHER FUCKER");
 insultes.push("COUILLES");
 insultes.push("COUILLE");
 insultes.push("ZIZI");
 insultes.push("ZEZETTE");
 insultes.push("KIKINE");

					var tempMessage = message;				
					var motsMessages = tempMessage.split(" ");
					var indexInsulte = -1;
					
					for (i = 0; i < motsMessages.length; i++)
					{
						indexInsulte = insultes.indexOf(motsMessages[i].normalizeAcc().replace(/[\u0300-\u036f]/g, "").toUpperCase());
						
						if (indexInsulte > -1)
						{		
							var censure = "";
							for (j = 0; j < insultes[indexInsulte].length; j++)
							{
								censure += "*";
							}
							motsMessages[i] = censure;	
						}
					}
					
					return motsMessages.join(" ");	
}

function newAnnonceUser(data)
{

	if (data.user == IDConnected)
	{

		if (data.type == 3)
		{
			if (!isMuted) audioBonneReponse.play();
			
			if (data.annonce.indexOf("Bonne réponse ! ") == 0)
			{
				$("#chatbox > .message").last().remove();
			}
		}	

		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
		$("#chatbox").scrollTop(99999);
		
	}
}

function applaudissement_send(data)
{
	if (data.user == IDConnected)
	{
		
		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
		$("#chatbox").scrollTop(99999);
		
		if (!isMuted)
		{
			audioApplause[nextApplause()].play();
		}
			
		$(".player[uuid='"+data.user+"'] > .player-pseudo").addClass('shake-slow');		

		setTimeout(function(){
			
			$(".player[uuid='"+data.user+"'] > .player-pseudo").removeClass('shake-slow');	
			
		},3900);
	}
}

function applaudissement_get(data)
{
	$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
	$("#chatbox").scrollTop(99999);
	
	
	
		if (!isMuted) audioApplause[nextApplause()].play();
	
		$(".player[uuid='"+data.user+"'] > .player-pseudo").addClass('shake-slow');	
		
		setTimeout(function(){
			
			$(".player[uuid='"+data.user+"'] > .player-pseudo").removeClass('shake-slow');	
			
		},3900);
}


function newAnnonceVocale(data)
{
	
	duree = data.secondes;

		setTimeout(function(){
			animParler();
			setTimeout(function(){
			animStop();
	
		},duree*1000);
		},1850);

	if (!isMuted)
	{
		setTimeout(function(){
			
			var annonceAudio = new Audio('https://directquiz.org/speech/' + data.annonce);
			annonceAudio.play();
			
		},1500);
	}
	
}

// BALISE VIDEO

function animParler() {
    // Lit la vidéo
    lecteur.play();
}

function animStop() {
    // Arrête la vidéo
    // On met en pause
    // On se remet au départ
    lecteur.currentTime = 0;
    lecteur.pause();

}

// -- BALISE VIDEO



function newAnnonce(data)
{
	var annonce = "";
	
	if (data.type != 111)
		annonce = decodeURIComponent(data.annonce.replace(regexHTML,""));
	else
		annonce = data.annonce;
	
	annonce = annonce.replace("お茶が好き","Oka ga suki");
	
	
	var dureeMS = 4000; //(data.annonce.length * 100) - 850;
	
	if (data.type == 4)
	{
		currentText = "";
		currentQuestionOrTimeOut = annonce;
		$("#questionbox-content").removeClass();
		
		if (annonce.indexOf("La bonne réponse était") > -1)
		{
			
			$("#questionbox").animate({			
					'background-color': '#f05274'
			},700,function(){
				
			});

		}
		else
		{
			if (annonce.indexOf("QUESTION") > -1)
			{
				if (!isMuted) audioQuestion.play();
				$("#questionbox").animate({			
					'background-color': '#a12e7a'
				},700,function(){
						
				});
			}
			else
			{
					$("#questionbox").animate({			
						'background-color': '#ba348d'
					},700,function(){
						
					});
					
					if (annonce.indexOf("Une partie en mode ") > -1)
					{
						if (!isMuted) 
						{
							setTimeout(function(){
								
								audioIntroGame.play();
								
							},3000);
							
						}
					}
			}

		}

		displayTexteProgressif();			
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											//animParler();
		
		/*
		if (annonce.length <= 10)
		{
			dureeMS = 900;
		}
		*/
		
		if (annonce.indexOf("La bonne réponse était") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}

		
	}
	else if (data.type == 3)
	{
		if (!isMuted) audioBonneReponse.play();
	}
	else if (data.type == 5)
	{
		if (annonce.lastIndexOf(pseudoClient+" ") > -1)
		{
			location.reload();
		}
	}
	else if (data.type == 99)
	{
		
		if (annonce.indexOf("La bonne réponse était") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}
		
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											
											//animParler();
		
	}
	else if (data.type == 98)
	{
		
		setTimeout(function(){
			
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'> <img src='images/icon-presenter"+(currentSalon == 2?"_F":"")+".png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>" + annonce + "</p></div>");
			$("#chatbox").scrollTop(99999);
			
		},1900);

		//animParler();
		
	}
	
	if (data.type != 98)
	{
		if (getTypeMessage(data.type) == "-info")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-info.png' alt='' class='message-info-img'/> <p class='message-info-label'>"+annonce+"</p></div>");		
		}
		else if (getTypeMessage(data.type) == "-connect")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-connect.png' alt='' class='message-connect-img'/> <p class='message-connect-label'>"+annonce+"</p></div>");		
		}
		else if (getTypeMessage(data.type) == "-disconnect")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-disconnect.png' alt='' class='message-disconnect-img'/> <p class='message-disconnect-label'>"+annonce+"</p></div>");		
		}
		else
		{	
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + annonce + "</div>");	
		}
		
		$("#chatbox").scrollTop(99999);
		
		if (data.type == 1) // connexion
		{
			if (!isMuted) audioConnected.play()
		}
		else if (data.type == 2) // déconnexion
		{
			if (!isMuted) audioDisconnected.play()
		}
	}
	
	$( "img" ).load(function() {
			$("#chatbox").scrollTop(99999);	
	});
}

function presentateurParle(annonceParlee)
{		
		$("#chatbox").append("<div class='message"+getTypeMessage(98)+"'> <img src='images/icon-presenter"+(currentSalon == 2?"_F":"")+".png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>" + annonceParlee + "</p></div>");
		$("#chatbox").scrollTop(99999);
}

function newMessage(data)
{
	
	if (!isMuted) audioMsg.play();
	
	var message = "";
	
	if (data.message.indexOf('giphy.com/media/') > -1)
	{
		 message = data.message;
	}
	else
	{
		 message = data.message.replace(regexHTML,"");
	}	
	
	$("#chatbox").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+mili()+"' class='microavatar' /> <strong>" + (data.pseudo.split("#")[1] == "1"?data.pseudo.replace("#1",""):data.pseudo) + "</strong> : " + message + "</div>");
	
	$( ".gif" ).load(function() {
			$("#chatbox").scrollTop(99999);	
	});
	
	$("#chatbox").scrollTop(99999);	
}

function mili()
{
	
	var min = 500;
	var max = 1024;
		
 return Math.floor(Math.random() * (max - min + 1)) + min;
}

$(function(){
	
/*
	if (currentSalon == 1)
	{
		setTimeout(function(){
			
				setInterval(function(){
					
					$.ajax({
					url: "https://www.directquiz.org/ping.php",
						error: function(jqXHR, textStatus, errorThrown){					
					
							if (textStatus == 'error')
							{
									isOnline = false;
									$("#chatbox").append("<div class='message"+getTypeMessage(2)+"'> <img src='images/icon-disconnect.png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>Connexion au serveur perdue, tentative de reconnexion...</p></div>");
									$("#chatbox").scrollTop(99999);
							}
						
					},
						success: function(data){					
											
							if (!isOnline)
							{
								isOnline = true;
								$("#chatbox").append("<div class='message"+getTypeMessage(1)+"'> <img src='images/icon-connect.png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>Connexion retrouvée, reconnexion...</p></div>");
								$("#chatbox").scrollTop(99999);
								setTimeout(function(){
								connectToServer(currentpseudonyme, currentUUID, currentSalon, currentclientID);
								},4000);
							}
						
					},
						timeout: 2000 // sets timeout to 2 seconds
					});				
						
				},6000);
				

					
		},1000);
	}
*/

// Gestion du realtime typing current

	$("#message-send").keypress(function () {
		if (currentSalon > 0)
		{
			  clearTimeout(typingTimer);
			  if (!isTyping) 
			  {
					var dataWriting = {
					uuid: currentUUID,
					state: 1
					};	
					
				socket.emit('currentWriting',dataWriting);
				isTyping = true;
			  }
		}
	  
	});

	$("#message-send").keyup(function () {
		if (currentSalon > 0)
		{
			  clearTimeout(typingTimer);
			  typingTimer = setTimeout(function () {
				if (isTyping) {
					var dataWriting = {
					uuid: currentUUID,
					state: 0
					};	
					
				 socket.emit('currentWriting',dataWriting);
				 isTyping = false;
				}
			  }, 900);
		}
	});
	
});


function refreshStatusWriting(data)
{
	statusText = "";
	
	console.log("DATA : ==" + data.id[0] + "==" +data.id[1] +"==");

	var listeFinale = new Array(0);

	for (i = 0; i < data.id.length; i++)
	{
		if (data.id[i] != currentUUID) listeFinale.push(data.id[i]);
	}
	
	for (i = 0; i < listeFinale.length; i++)
	{
		
		var separateur = ", ";
		
		if (i == listeFinale.length-2)
		{
			separateur = " et ";
		}
		
		if (!(i == listeFinale.length-1))
		{
			statusText += $(".player[uuid='"+listeFinale[i]+"']").children(".player-pseudo").text() + separateur;
		}
		else
		{
			statusText += $(".player[uuid='"+listeFinale[i]+"']").children(".player-pseudo").text();
		}
		
	}

	var verbe = "est";
	
	if (listeFinale.length > 1) verbe = "sont";
	
	if (listeFinale.length > 0)
	$("#writingCurrentContainer").html("<img src='images/loadingWriting.gif'>&nbsp;<span>" + statusText + " " + verbe + " en train d'écrire...</span>");
	else
	$("#writingCurrentContainer").html("");

}

function newMessageClient(data)
{
	
		if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") > -1)
		{
			var GIF = data.message.split(/\|/);
			$("#chatbox").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+mili()+"' class='microavatar' /> <strong>" + data.pseudo + "</strong> : " + "<img class='gif' src='"+GIF[1]+"' />" + "</div>");
			
			var dataGIF = {
				id: IDConnected,
				pseudo: pseudoConnected,
				message: "<img class='gif' src='"+GIF[1]+"' />"
			};

			socket.emit('message',dataGIF);
			
			$( ".gif" ).load(function() {
				$("#chatbox").scrollTop(99999);	
			});
		}
		else
		{	
	
		
				var message = nettoyerMessageDeToutesSesInsultes(data.message.replace(regexHTML,""));	
				
				if (message == "/cls") $("#chatbox").html("");
				
				if (message.indexOf("/setvoice") == 0)
				{
					if (message.indexOf(" femme") > -1)
					{
						//setFemme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix de femme !");
					}
					else if (message.indexOf(" homme") > -1)
					{
						//setHomme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix d'homme !");
					}
				}
				else if (message.indexOf("/gif") == 0)
				{
					var query = message.replace("/gif",""); 
					if (query != "") searchGIF(query);	
				}
				else
				{
					//$(".slot[data-user-id="+IDConnected+"]").children(".pseudonyme").eq(0).text()
					if (message.indexOf("/start") == -1)
					{
						$("#chatbox").append("<div class='message'><img src='images/avatar/"+IDConnected+".jpg?"+mili()+"' class='microavatar' /> <strong>" + dataMember.pseudo + "</strong> : " + message + "</div>");
						if (!isMuted) audioMsg.play();
					}
					
				}
				$("#chatbox").scrollTop(99999);	
		}
	
	
}

function itemClassement(uuid, points)
{
	this.uuid = uuid;
	this.points = points;
}

function newListeSlotsClassement(data)
{

	points = data; // uuid, points
	
	reorganiserClassement(true);

}

function notifyPoints(data)
{

	if (dataMember.id == data.uuid)
	{
		$("#score-good-answer-points").text("+"+data.points)
		
		$("#score-good-answer").show('bounce', {}, 250,function(){});
		
		setTimeout(function(){
			
			$("#score-good-answer").fadeOut(250);
			
		},2800)
		
		
	}
}

function notifyOrdre(data)
{

	var uuidToSet = data.uuid;
	var ordreToSet = data.ordre;

	$(".player[uuid='"+uuidToSet+"']").addClass("player-good-answer");
	$(".player[uuid='"+uuidToSet+"']").children(".player-good-answer-notification").text(ordreToSet);
	$(".player[uuid='"+uuidToSet+"']").children(".player-good-answer-notification").show();
	
}

function resetAffichageClassement(data)
{
	$(".player").each(function(){
		
			$(this).removeClass("player-good-answer");
			$(this).children(".player-good-answer-notification").hide();
		
	});
}

function newListeSlots(data)
{
	var UUIDAdmin = "514c0686-1d07-4f44-aa21-01f44c8a00be";
	var htmlClassementContent = "";
	
	points = new Array(0);
	
	for (i = 0; i < data.length; i++)
	{

		htmlClassementContent += '<div style="position:absolute;" uuid="'+data[i].id+'" class="player">'+
							 '<div style="display:none;" class="player-good-answer-notification"></div>'+
							 '<img src="images/avatar/'+data[i].id+'.jpg" alt="" uuid="'+data[i].id+'" class="player-avatar">'+
							 '<img src="images/avatar/level_1.png" uuid="'+data[i].id+'" class="avatar-level" />'+
							 '<p class="player-pseudo">'+ (data[i].id == UUIDAdmin?'<i class="couronne" title-data="ADMIN" style="font-style: normal; cursor: help;">👑</i>':'') +' '+(data[i].version == 1?data[i].name:data[i].name+"#"+data[i].version)+'</p>'+
						     '<span class="pts-negatif"> '+data[i].points+' </span>'+
						     '</div>';
	
		points.push(new itemClassement(data[i].id, data[i].points));		 
							 
	}
	
	$("#score").html("");
	$("#score").append("<div id='classement-liste'>" + htmlClassementContent + "</div>");
	$("#score").append('<div style="display:none" id="score-good-answer">'+
					   '<span id="score-good-answer-points"></span>'+
					   '<span id="score-good-answer-label">Points</span>'+
					   '</div><div style="display:none;" id="tooltip"></div>');
				
	$("#classement-liste").css("position:relative;");

	
				$(".couronne").mousemove(function(event){

					$("#tooltip").text($(this).attr("title-data"));
					
					$("#tooltip").offset({ top: event.pageY+15, left: event.pageX+15 });
				
				});
				
				$(".couronne").mouseleave(function(event){
					
					$("#tooltip").hide();
					
				});
				
				$(".couronne").mouseenter(function(event){
					
					$("#tooltip").show();
					$(this).effect( "highlight", {}, 400, function(){ } );
					
				});
	
	
	
	// ranksTop contient dans l'ordre du classement les positions top 
	// ranksLeft contient dans l'ordre du classement les positions left
	ranksTop = new Array(0);
	ranksLeft = new Array(0);
	
	for (i = 0; i < points.length; i++)
	{
		var rankIndex = i + 1;
		
		var ranktop = 0
		var rankleft = 0
		
		if (rankIndex >= 1 && rankIndex <= 4) rankleft = 0;
		if (rankIndex >= 5 && rankIndex <= 8) rankleft = 209;
		
		switch(rankIndex)
		{
			case 1:ranktop=0; break;
			case 2:ranktop=43; break;
			case 3:ranktop=86; break;
			case 4:ranktop=129; break;
			case 5:ranktop=0; break;
			case 6:ranktop=43; break;
			case 7:ranktop=86; break;
			case 8:ranktop=129;
		}
		
		ranksTop.push(ranktop);
		ranksLeft.push(rankleft);
	}	
					   
	reorganiserClassement(false);				   


}


function reorganiserClassement(isAnimated)
{

	pointsOrdre = points.map((x) => x);
	
	pointsOrdre.sort(function (a, b) {
			return b.points - a.points;
	});
	
		rankingRestant = points.length;
		processReorganisation(rankingRestant, rankingRestant, isAnimated);
	
	refreshAvatars();
	
}

function getPositionClassementByTopLeft(top_, left_)
{
	
	if (top_ == 0)
	{
			if (left_ == 0)
			{
				return 1;
			}
			else if (left_ == 209)
			{
				return 5;
			}
	}
	else if (top_ == 43)
	{
			if (left_ == 0)
			{
				return 2;
			}
			else if (left_ == 209)
			{
				return 6;
			}
	}
	else if (top_ == 86)
	{
			if (left_ == 0)
			{
				return 3;
			}
			else if (left_ == 209)
			{
				return 7;
			}
	}
	else if (top_ == 129)
	{
			if (left_)
			{
				return 4;
			}
			else if (left_ == 209)
			{
				return 8;
			}
	}
	
}

function processReorganisation(restant, total, isAnimated)
{
	
		if (restant > 0)
		{
			
			var indexItem = (total-(restant-1))-1;

			// uuidCurrent est l'UUID de l'utilisateur au fur et à mesure de l'ordre du classement final dans l'ordre
			var uuidCurrent = pointsOrdre[indexItem].uuid;
			
			// Placement au premier plan de l'item courant 
			$(".player[uuid='"+uuidCurrent+"']").css("z-index",999);
			
			// currentTop et currentLeft sont la position actuelle des utilisateurs au fur et à mesure dans l'ordre du classement final
			var currentTop = $(".player[uuid='"+uuidCurrent+"']").position().top;
			var currentLeft = $(".player[uuid='"+uuidCurrent+"']").position().left;
			
			var rank = getPositionClassementByTopLeft(currentTop, currentLeft);
			
			if (isAnimated)
			{
				
				// Animation du player dont l'uuid est celui à classer en première position et ainsi de suite (basé sur pointsOrdre)
				
				// ranksTop/Left = La position que l'on souhaite appliquer en fonction du rang voulu
				// Par rapport au RANK actuel qui est calculé grace au top et au left actuel
				$(".player[uuid='"+uuidCurrent+"']").animate({
						top: ranksTop[indexItem], //- (43*(rank-1)) + (rank >= 5 && rank <= total?172:0),
						left : ranksLeft[indexItem] //- (rank >= 5 && rank <= total?209:0)
					}, 500, function() {
	
				});	
				
				//
				//rank >= 5 && rank <= total?(-209):0)
				
			}
			else
			{
				// identique à l'animation mais sans animation
				$(".player[uuid='"+uuidCurrent+"']").css("top",ranksTop[indexItem]).css("left",ranksLeft[indexItem]);
			}


			// - (43*(rank-1)) + (rank >= 5 && rank <= total?172:0)
			//  + (rank >= 5 && rank <= total?(-209):0)

			restant--; // décrémentation au fur et à mesure (normalement pas de bug)
			processReorganisation(restant, total, isAnimated); //Recursivité (normalement ok)
			
		}
		else
		{
			
			// Attribution des points en fin d'animation, normalement aucun soucis de bugs...			
			for (i = 0; i < pointsOrdre.length; i++)
			{
				$(".player[uuid='"+pointsOrdre[i].uuid+"']").children("span").text(pointsOrdre[i].points);		
			}			

			return;
		}
}