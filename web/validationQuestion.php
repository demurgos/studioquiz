<?php session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz</title>
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	<!-- Datatable CSS -->
    <link rel="stylesheet" href="css/data-table.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

	<script>
	
	$(function(){
		
		
				
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
						
						
						
					}).
					fail(function(){
						
						
						
					});

		
		setJqueryDataTable();
		
	});
	
	function refreshDirectDollar()
	{
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}

						
					}).
					fail(function(){
						
						
						
					});
	}
	
	function validerQuestion(moimeme)
	{
		var q = $(moimeme).parent().parent().children("td").eq(1).text();
		
		if (confirm("Etes-vous sûr de vouloir approuver cette question : \""+q+"\" ?"))
		{
		
			$.post("accepterThis.php",{question:q}).done(function(data){
				
				if (data.trim() == "OK")
				{
					$(moimeme).parent().parent().remove();
					alert("Question approuvée avec succès !");
				}
				
			});
		
		}
		
	}
	
	function refuserQuestion(moimeme)
	{
		var q = $(moimeme).parent().parent().children("td").eq(1).text();
		
		if (confirm("Etes-vous sûr de vouloir refuser la question : \""+q+"\" ?"))
		{
		
			$.post("refuserThis.php",{question:q}).done(function(data){
				
				if (data.trim() == "OK")
				{
					$(moimeme).parent().parent().remove();
					alert("Refusé avec succès !");
				}
				
			});
		
		}
		
	}
	
	function checkQuestion(moimeme)
	{
		
		$("#modale-corr").show();
		
		var q = $(moimeme).parent().parent().children("td").eq(1).text();

			$.post("checkCorrespondances.php",{question:q}).done(function(data){
				
				var finalData = data.replace(/\|###EOF###/,"");
				var arrayListe = finalData.split(/\|/);
				
				$("#liste-corr").html("");
				
				for (i = 0; i < arrayListe.length; i++)
				{
					$("#liste-corr").append("<li>"+arrayListe[i]+"</li>");		
				}					
				
			});

		
	}
	
	function fermerModale()
	{
		$("#liste-corr").html("");
		$("#modale-corr").hide();
	}
	
	
	
	
	function soumettreQuestion()
	{

		var question_ =	$("#txtQuestion").val().trim();

		var tags_ = new Array(0);
		var reponses_ = new Array(0);


				for (i = 1; i<= 4; i++)
				{
					if ($("#txtReponse"+i).val().trim() != "")
					{
						reponses_.push($("#txtReponse"+i).val().trim().replace(/\|/,""));
					}
					
					if ($("#txtTag"+i).val().trim() != "")
					{
						tags_.push($("#txtTag"+i).val().trim().replace(/\|/,""));
					}
				}

					if ($("#txtTag5").val().trim() != "")
					{
						tags_.push($("#txtTag5").val().trim().replace(/\|/,""));
					}

				var reponsesF_ = reponses_.join("|");

				var tagsF_ = tags_.join("|");

				var diff_ =	$("#ddlDifficulte option:selected").val();


				if (question_ != "" && reponsesF_ != "" && tagsF_ != "")
				{

						$.post("depense.php", {uuid:'<?= $_SESSION['userid_dq'] ?>', depense:0}).done(function(data){
			
							if (data.trim() == 'OK')
							{
								$.post("insertQuestion.php",{question:question_, reponses:reponsesF_, tags:tagsF_, diff:diff_, uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){
						
									alert(data);
									
									$(".fields").val("");	
									
								});
								refreshDirectDollar()
							}
							else
							{
								alert("Vous n'avez pas assez de DirectDollar pour soumettre une question...");
							}
						
						});
							
				}
				else
				{
					alert("Veuillez compléter les informations manquantes...");
				}
					

		

	}
	
	  function setJqueryDataTable() {

            $("#questionToValidate").DataTable({
				 language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/French.json'
        },
                pageLength: 25,
                order: [1, "asc"],
                aoColumnDefs: [
                {
                    "targets": [0],
                    "orderable": true,
                    "searchable": true,
					"width" : 100
                },
			    {
                    "targets": [1],
                    "orderable": true,
                    "searchable": true,
                    "width" : 350
                },
                {
                    "targets": [2],
                    "orderable": true,
                    "searchable": true,
                    "width" : 200
                },
                {
                    "targets": [3],
                    "orderable": true,
                    "searchable": true,
                    "width" : 200
                },
                {
                    "targets": [4],
                    "orderable": true,
                    "searchable": false,
                    "width" : 50
                }
			    
                ],

                dom: 'Brftip'

            });

        }
		
		</script>
	
</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
                        <li><a class="nav-link" href="about.php">A propos</a></li>
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link active" href="validationQuestion.php">Proposer</a></li>
						
					   <li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
						 <li><a class="nav-link" href="dons.php">Faire un don</a></li>
					   </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->


    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">PROPOSITIONS </span>DE QUESTIONS</h2>
                            <p class="large">Les futures questions de DirectQuiz</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">


                <div class="col-lg-12 col-md-12 col-sm-12 white_fonts">
             
			 
			 <h2>Proposer une question</h2>
			 <table id="table-proposer">
				<tr><td class="celulle">Question</td><td><input type="text" placeholder="Intitulé de la question" class="fields" style="width:600px;" id="txtQuestion" /></td></tr>
				<tr><td class="celulle">Réponse(s) possible(s)</td><td><input type="text" placeholder="Réponse 1" class="fields" id="txtReponse1" /><br/><input type="text" placeholder="Réponse 2" class="fields" id="txtReponse2" /> <i>(facultatif)</i> <br/><input type="text" placeholder="Réponse 3" class="fields" id="txtReponse3" /> <i>(facultatif)</i> <br/><input type="text" placeholder="Réponse 4" class="fields" id="txtReponse4" /> <i>(facultatif)</i> </td></tr>
				<tr><td class="celulle">Catégorie(s)</td><td><input type="text" placeholder="Catégorie 1" class="fields" id="txtTag1" /><br/><input type="text" placeholder="Catégorie 2" class="fields" id="txtTag2" /> <i>(facultatif)</i><br/><input type="text" placeholder="Catégorie 3" class="fields" id="txtTag3" /> <i>(facultatif)</i><br/><input type="text" placeholder="Catégorie 4" class="fields" id="txtTag4" /> <i>(facultatif)</i><br/><input type="text" placeholder="Catégorie 5" class="fields" id="txtTag5" /> <i>(facultatif)</i></td></tr>
				<tr><td class="celulle">Difficulté</td><td>
				
				<select id="ddlDifficulte">
					<option value="0" style="background-color: cyan;">Ultra facile</option>
					<option value="8" style="background-color: #0F9;">Très facile</option>
					<option value="15" style="background-color: #3F0;">Facile</option>
					<option value="28" style="background-color: #FC0;">Moyenne</option>
					<option value="55" style="background-color: #F90;">Pas facile</option>
					<option value="65" style="background-color: #F30;">Difficile</option>
					<option value="75" style="background-color: #F00;">Très difficile</option>
					<option value="80" style="background-color: #900;">Compliquée</option>
					<option value="90" style="background-color: #300; color: white;">Très compliquée</option>
					<option value="100" style="background-color: #000; color: white;">Presqu'impossible</option>
				</select>
				</td></tr>
				<tr><td colspan="2"><input type="button" value="Soumettre" onClick="soumettreQuestion()" /> <span style="color:white;">(Elle sera soumise à validation)</span></td></tr>
			 </table>

						<?php
						include('config.php');
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}
							
								$query = "CALL DQ_GetRoleByUUID('".$_SESSION['userid_dq']."')";
								$result0 = $mysqli->query($query);			
						
								$role = $result0->fetch_array(MYSQLI_ASSOC);
								
								$roleFinal = $role['Role'];
						
								/* Libération des résultats */
								$result0->free();
						
														/* Fermeture de la connexion */
								$mysqli->close();
							
							
							$mysqli = null;
						
						if ($roleFinal == 5)
						{
							
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}
							
							echo '<p>
									<h2>Propositions de question</h2>';

								$query = "CALL DQ_GetQuestionsAValider()";
								$result = $mysqli->query($query);
								
								echo '<table id="questionToValidate" class="cell-border">';
								echo '<thead>';
								echo '<tr><th>Auteur</th><th>Question</th><th>Réponse(s)</th><th>Tags</th><th>% Difficulté</th><th></th></tr>';
								echo '</thead>';
								echo '<tbody>';
								/* Tableau associatif */
								while ($row = $result->fetch_array(MYSQLI_ASSOC))
								{
									echo '<tr><td><img class="avatar-classement" src="images/avatar/'.$row['AuteurUUID'].'.jpg?'.time().'" /> '.$row['Pseudo'].'</td><td>'.$row['question'].'</td><td>'.$row['answers'].'</td><td>'.$row['tags'].'</td><td>'.$row['difficulty'].'</td><td><input type="button" value="V" class="acceptation" onClick="validerQuestion(this)" /> <input type="button" class="refus" value="X" onClick="refuserQuestion(this)" /> <input type="button" class="checking" value="?" onClick="checkQuestion(this)" /></td></tr>';
								}

								echo '</tbody>';
								echo '</table>';

								/* Libération des résultats */
								$result->free();

								/* Fermeture de la connexion */
								$mysqli->close();
							
							
							$mysqli = null;
							
							echo '</p>';
							
						}
						?>
					
					
            
			<?php
	
			?>
			
                </div>
            </div>
        </div>
    </div>
    <!-- end section -->

	<div id="modale-corr">
	<span style="cursor:pointer; text-decoration: underline; float:right;" onClick="fermerModale()">FERMER [X]</span>
	<br/>
	<h2>Questions similaires</h2>
	<br/>
	<div style="overflow: auto; height:670px;">
		<ol id="liste-corr">
		
		</ol>
	</div>
	</div>
	
	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>