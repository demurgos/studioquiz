<?php
session_start();

$question = $_POST['question'];

include('config.php');

try
{

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	if (!$mysqli->query("Call DQ_InsertQuestionForDefinitive('". addslashes($question)."');")) {
		echo "Echec de la requête : " . $mysqli->error;
	}
	else
	{
		echo "OK";
	}
	
	$mysqli = null;

}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}


?>