<?php 
//session cross to sub domain
ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 
if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

$avatar_credential = rand(100,300);

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta https-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz - Jouer une partie</title>
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- DirectQuiz Choix Salon -->
    <link rel="stylesheet" href="css/style-sq-home.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
	<link rel="stylesheet" href="css/shaker.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
	
		
		#choixPrincipal
		{
			cursor: pointer;
			top: 50%;
			left: 50%; 
			transform: translate(-50%, -50%); 
		}
		
		.vignette-jouer
		{
			border-radius : 10px 10px 10px 10px;
			box-shadow: 0px 0px 10px #000;
			border: 5px solid white;
			margin-right: 10px;
			margin-top: 8px;
			width: 210px;
			height: 215px;
			cursor: pointer;
		}
		
		.vignette-jouer:hover
		{
			box-shadow: 0px 0px 10px #000;
			border: 5px solid #FE9;
		}
		
		#tooltip
		{
			background-color: #FBF;
			color: #007;
			padding: 5px;
			font-weight: bold;
			border-radius: 4px 4px 4px 4px;
			position: absolute;
			left: -100px;
			top: -100px;
		}

	</style>

	<script	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <script src="https://directquiz.niko.ovh/dev/js/jquery.tightgrid.js"></script>
	
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">


	<script>

function onlineEntry(theme, online)
{
	this.theme = theme;
	this.online = online;
}

var audioFlap  = new Audio('https://directquiz.niko.ovh/dev/flap.mp3');

var joueursOnline = new Array(0);

<?php include('niko.ovh/directquiz89/getAllJoueursOnlineByTheme.php') ?>

		$(function(){			

			$(".vignette-jouer").each(function(){
				
				var indexOfOnlineEntry = joueursOnline.map(function(e) { return e.theme; }).indexOf(parseInt($(this).attr("theme-id"),10));
				 
				if ($(this).attr("online") != "off")
				$(this).attr("online"," ("+joueursOnline[indexOfOnlineEntry].online + " / " + "8" + " joueur(s) en ligne)");
				
				$(this).mousemove(function(event){

					$("#tooltip").text($(this).attr("title-data") + (($(this).attr("online") != "off")?$(this).attr("online"):"") );
					
					$("#tooltip").offset({ top: event.pageY+15, left: event.pageX+15 });
				
				});
				
				$(this).mouseleave(function(event){
					
					$("#tooltip").hide();
					
				});
				
				$(this).mouseenter(function(event){
					
					$("#tooltip").show();
					
				});
	
			});  

			$("#jouer-megaquiz").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("megaquiz"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
			$("#jouer-spectacle").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("spectacle"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
			$("#jouer-jeuxvideo").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("jeuxvideo"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
			
			$("#jouer-histoire").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("histoire"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
			
			$("#jouer-geographie").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("geo"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
			
			$("#jouer-informatique").click(function(){

				audioFlap.play();

				var elementPrincipal = $(this).clone();

				$(elementPrincipal).attr("id","choixPrincipal");
				
				$(elementPrincipal).appendTo("#plateau");

				$(elementPrincipal).css("position","absolute");
				$(elementPrincipal).css("width","100px");
				$(elementPrincipal).css("height","102px");

				$(elementPrincipal).animate({	
					width: "600px",
					height: "614px"
				  }, 850, function() {
						// $(elementPrincipal).attr('onClick', 'openPartie("megaquiz")');
						
						setTimeout(openPartie("informatique"), 1900);
						
						/*
						$("#plateau").click(function() {
								$("#plateau").off('click');
								$("#choixPrincipal").fadeOut(400,function(){
									
									$("#choixPrincipal").remove();
									
								});				
						});
						*/
				  });
				
			});
			
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
			
		});
		
		function openPartie(mode)
		{
			document.location = mode+'.php';
		}
		
	</script>
	
<script src="https://directquiz.niko.ovh/dev/public/quiz.js"></script>
	
</head>

<body id="about_us" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

<div id="tooltip"></div>

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
                        <li><a class="nav-link" href="about.php">A propos</a></li>
						<li><a class="nav-link active" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					 
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
						  <li><a class="nav-link" href="dons.php">Faire un don</a></li>
                    </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->
	

    <!-- section -->
    <div id="fond-public" class="section layout_padding theme_bg">
        <div class="container">
        
<div id="my-toast-location" style="position: absolute; top; 10px; left: 10px;"></div>
<div id="sender">


<div id="plateau">
<center><h1>Choix du mode de jeu</h1></center>
<div style="margin-left: auto; margin-right: auto; max-width:670px;">
<a href="#"><img class="vignette-jouer" id="jouer-megaquiz" online="" theme-id="1" title-data="Megaquiz" alt="Megaquiz" src="images/megaquiz.png" /></a>
<a href="#"><img class="vignette-jouer" id="jouer-spectacle" online="" theme-id="2" title-data="Spectacle" alt="Spectacle" src="images/spectacle.png" /></a>
<a href="#"><img class="vignette-jouer" id="jouer-jeuxvideo" online="" theme-id="3" title-data="Jeux vidéo" alt="Jeux vidéo" src="images/jeuxvideo.png" /></a>
</div>
<br/>
<div style="margin-left: auto; margin-right: auto; max-width:670px;">
<a href="#"><img class="vignette-jouer" id="jouer-histoire" online="" theme-id="4" title-data="Histoire" alt="Histoire" src="images/histoire.png" /></a>
<a href="#"><img class="vignette-jouer" id="jouer-geographie" online="" theme-id="5" title-data="Géographie" alt="Géographie" src="images/geo.png" /></a>
<a href="#"><img class="vignette-jouer" id="jouer-informatique" online="" theme-id="6" title-data="Informatique" alt="Informatique" src="images/informatique.png" /></a>
</div>

	<div id="cadreDefilement"><marquee direction="left"><p id="defilement">Directquiz : Le quiz en DIRECT ! Venez participer dans la catégorie de votre choix ! Testez votre culture générale sur 10 questions...</p></marquee></div>

</div> <!-- plateau -->
</div> <!-- sender -->

<br/><br/>


               
      
        </div>
    </div>
    <!-- end section -->

	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script src="https://j-talk.com/js/app.js?v=1.3.0-beta"></script>
</body>

</html>