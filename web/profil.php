<?php 
//session cross to sub domain
ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

if (!isset($_SESSION['userid_dq']))
{
	header('Location: index.php'); 
}

?>
<!DOCTYPE html>
<html lang="fr"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Directquiz - Gestion du profil</title> 
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />	
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

		<!-- ALL JS FILES -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	
	
	
	<script>
	
	$(function(){
		
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture").html("<img class='ceinture-profil' src='images/ceinture_"+result[0].trim()+".png' />");
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
							$("#XP").text(result[2].trim());
							
																				
							$("#profil-thune-cadre").html("Fortune : " + result[3].trim()+" <img class='piecetteG' title='DirectDollar' src='images/dd.png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
		
	});
		
	</script>
	
</head>
<body id="contact_page" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

	<!-- LOADER -->
    <div id="preloader">
		<div class="loader">
			<img src="images/loader.gif" alt="#"/>
		</div>
    </div><!-- end loader -->
    <!-- END LOADER -->
	
	<!-- Start header -->
	<header class="top-header">
		<nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
				<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
                        <li><a class="nav-link" href="about.php">A propos</a></li>
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
						<li><a class="nav-link active" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
						 <li><a class="nav-link" href="dons.php">Faire un don</a></li>
                    </ul>
                </div>
				<div class="search-box">

	             </div>
            </div>
        </nav>
	</header>
	<!-- End header -->

	    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">GESTION</span> DU PROFIL</h2>
                            <p class="large"><?= $_SESSION['pseudo_dq'] ?></p>
											
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 white_fonts">

									<table id="table-profil">
						<tr><td colspan="2" id="bandeau-profil" style="text-align : center;"><span id="pseudo-profil"><?= $_SESSION['pseudo_dq'] ?></span> <span id="ceinture" class="ceinture"></span>  | XP : <span id="XP"></span> </td></tr>
						<tr><td colspan="2"><div id="profil-thune-cadre"></div></td></tr>
						<tr><td>Avatar actuel : </td><td style="padding-left:30px;"><br/><br/><img id="currentAvatarGestion" src="images/avatar/<?= $_SESSION['userid_dq'] ?>.jpg?<?= time(); ?>"></img></td></tr>
						<tr><td>Choisir un nouvel avatar : </td><td style="padding-left:30px;">
				<br/><br/>
				    <form action="profil.php" method="post" enctype="multipart/form-data">
 
        <label for="fileSelect">Fichier :</label>
        <input type="file" name="photo" id="fileSelect">
        <input type="submit" name="submit" value="Upload">
        <p><strong>Note:</strong> Format accepté : .jpg | Taille maximum : 5 Mo</p>
    </form>
	<?php
	// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpeg");
        $filename = $_SESSION['userid_dq'].".jpg"; //$_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Erreur : Sélectionnez un format valide.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Erreur : La taille du fichier ne peut pas dépasser 5 Mo.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("images/avatar/" . $filename)){
                unlink("images/avatar/" . $filename);
            } 
               
			   $x = 256;
			   $y = 256;
			    move_uploaded_file($_FILES["photo"]["tmp_name"], "images/avatar/" . $filename);
				$chemin = "images/avatar/" . $filename;			   
				$img_new = imagecreatefromjpeg($chemin);
				$size = getimagesize($chemin);
				$img_mini = imagecreatetruecolor ($x, $y);
				imagecopyresampled ($img_mini,$img_new,0,0,0,0,$x,$y,$size[0],$size[1]);
				imagejpeg($img_mini, "images/avatar/" . $filename);

							   
                echo "Avatar envoyé avec succès !";
             
        } else{
            echo "Erreur : Le type de fichier envoyé n'est pas autorisé."; 
        }
    } else{
        echo "Erreur : " . $_FILES["photo"]["error"];
    }
}
?>

						</td></tr>
					</table>
						<br/>

				
                </div>
            </div>
        </div>
    </div>
    <!-- end section -->
	
	
	<?php 
	
		include('footer.php');
	
	?>


	
	<a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

	


	

    <!-- ALL PLUGINS -->
	<script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script> 
	<script src="js/slider-index.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
	<script src="js/isotope.min.js"></script>	
	<script src="js/images-loded.min.js"></script>	
    <script src="js/custom.js"></script>
</body>
</html>